package com.miaoyi.controller;


import cn.hutool.core.bean.BeanUtil;
import com.miaoyi.entity.WxCommonUser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UtilsController {

    //todo map转对象 对象转map
    public static void main(String[] args)  {
        WxCommonUser user= new WxCommonUser();
        user.setId(1L);
        user.setNickName("三省同学");
        //java转map
        //可以用于存储redis hash结构
        Map<String, Object> map1 = BeanUtil.beanToMap(user); //不忽略null值
        System.out.println(map1);

        //map转java
        Map<String, Object> map = new HashMap();
        map.put("id", 2L);
        map.put("nickName", "三省同学2");
        System.out.println(BeanUtil.toBean(map, WxCommonUser.class));
    }



}
