package com.miaoyi;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//一些常用的工具类 用法
public class CommonUtils {

    public static void main(String[] args) {
        SpringApplication.run(CommonUtils.class, args);
    }

}
