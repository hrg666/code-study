package com.miaoyi.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxLotteryPrize implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer id;

    private String actId;
    private Integer prizeLevel;

    private String prizeName;

    private String prizeImg;

    private Integer prizeTotal;




}
