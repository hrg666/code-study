package com.miaoyi.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxCommonUser implements Serializable {

    private static final long serialVersionUID=1L;


    private Long id;

    private String actId;

    private String openid;
    private String nickName;


    private String sex;

    private String city;




}
