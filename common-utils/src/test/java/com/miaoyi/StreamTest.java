package com.miaoyi;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.miaoyi.entity.WxCommonUser;
import com.miaoyi.entity.WxLotteryPrize;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class StreamTest {



    //判断是否list 对象里 有某个符合的值
    @Test
    void test1() {
        WxLotteryPrize wxLotteryPrize1 = new WxLotteryPrize();
        wxLotteryPrize1.setActId("1");
        wxLotteryPrize1.setId(1);
        WxLotteryPrize wxLotteryPrize2 = new WxLotteryPrize();
        wxLotteryPrize2.setActId("1");
        wxLotteryPrize2.setId(1);

        List<WxLotteryPrize> list = new ArrayList<>();
        list.add(wxLotteryPrize1);
        list.add(wxLotteryPrize2);


       // boolean isExist = list.stream().map(WxLotteryPrize::getId).anyMatch(1);
        boolean anyMatchFlag = list.stream().anyMatch(lotteryPrize -> lotteryPrize.getId() == 2);
        boolean anyMatchFlag2 = list.stream().anyMatch(lotteryPrize -> lotteryPrize.getActId().equals("2"));

        System.out.println(anyMatchFlag);
        System.out.println(anyMatchFlag2);
    }

}
