package com.miaoyi;

import cn.hutool.core.bean.BeanUtil;


import cn.hutool.core.bean.copier.CopyOptions;
import com.miaoyi.entity.WxCommonUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class UtilsTest {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //对象转map
    @Test
    void test1() {
        WxCommonUser user= new WxCommonUser();
        user.setId(1L);
        user.setNickName("三省同学");
        //java转map
        //可以用于存储redis hash结构
        Map<String, Object> map1 = BeanUtil.beanToMap(user);
        System.out.println(map1);


        Map<String,Object> userMap = BeanUtil.beanToMap(user,new HashMap<>(),
                new CopyOptions().setIgnoreNullValue(true).setFieldValueEditor(((fieldName, fieldValue) -> {
                    if (fieldValue!=null) {
                        return fieldValue.toString();
                    }
                    return "null";
                })));
        stringRedisTemplate.opsForHash().putAll("key",userMap);//map得都是string类型

        //map转java
       /* Map<String, Object> map = new HashMap();
        map.put("id", 2L);
        map.put("nickName", "三省同学2");
        System.out.println(BeanUtil.toBean(map, WxCommonUser.class));*/
    }

}
