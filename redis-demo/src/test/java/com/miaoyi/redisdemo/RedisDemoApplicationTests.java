package com.miaoyi.redisdemo;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

//@SpringBootTest
class RedisDemoApplicationTests {


    @Test
    void contextLoads() {
        //原生版
        // 1:创建Jedis连接池
        Jedis jedis = new Jedis("localhost", 6379);
        //执行对于的命令
        String wolfcode = jedis.set("wolfcode", "66");
        System.out.println(wolfcode);
        //关闭资源
        jedis.close();
    }

    @Test
    void testTool() {
        //使用连接池
        //创建一个连接池对象
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxTotal(20);//最大连接数 默认为8
        config.setMaxIdle(20); //最大空闲 默认为8
        JedisPool pool = new JedisPool(config,"localhost");
        //从连接池获取连接对象
        Jedis jedis = pool.getResource();
        String wolf = jedis.set("wolf", "77");
        System.out.println(wolf);
        jedis.close(); //把连接归还到连接池
        pool.close(); //关闭连接池
    }


}
