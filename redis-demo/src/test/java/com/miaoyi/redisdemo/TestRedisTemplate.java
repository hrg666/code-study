package com.miaoyi.redisdemo;

import com.alibaba.fastjson.JSON;
import com.miaoyi.redisdemo.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

//@SpringBootTest
public class TestRedisTemplate {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    void testTemplate() {
        //操作String
        //存入键值对
        redisTemplate.opsForValue().set("xioaming", "66");
    }

    @Test
    void testTemplate2() {
        //获取
        String xiaoming = redisTemplate.opsForValue().get("xioaming");
        System.out.println(xiaoming);
    }

    @Test
    void testTemplate9() {
        //把值增加1 默认从1开始
        redisTemplate.opsForValue().increment("huangrui");

        //把值增减1
        // redisTemplate.opsForValue().decrement("xioaming");

        //删除键值对
        Boolean xioaming = redisTemplate.delete("xioaming");
    }

    @Test
    void testTemplate10() {
        //存入键值对 失效时间
        redisTemplate.opsForValue().set("age", "66", 1, TimeUnit.MINUTES);
        redisTemplate.opsForValue().set("sex", "1", 7, TimeUnit.DAYS);
    }

    @Test
    void testTemplate11() {
        //过期时间
        //存入键值对
        //redisTemplate.opsForValue().set("age3","6336");
        redisTemplate.expire("age3", 3, TimeUnit.MINUTES);
    }

    @Test
    void testTemplate3() {
        //操作hash
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", "小明");
        map.put("age", "18");
        redisTemplate.opsForHash().putAll("key2", map);
    }

    @Test
    void testTemplate12() {
        //redisTemplate.opsForHash().put("key2","age1","15");
        User user = new User();
        user.setAge(14);
        user.setId(14L);
        user.setName("黄瑞国");
        String s = JSON.toJSONString(user);
        redisTemplate.opsForHash().put("key2", user.getId() + "", s);
    }

    @Test
    void testTemplate13() {
        //String o = (String) redisTemplate.opsForHash().get("key4", "age");
        String o = (String) redisTemplate.opsForHash().get("key2", "14");
        User user = JSON.parseObject(o, User.class);
        System.out.println(user);
        System.out.println(o);
    }

    @Test
    void testTemplate14() {

    }

    @Test
    void testTemplate4() {
        //操作list
        //redisTemplate.opsForList().leftPushAll("key5","3","4","5");
        //redisTemplate.opsForList().set("key5",0,"8989");
        redisTemplate.opsForList().rightPush("key5", "312312");
        redisTemplate.opsForList().leftPush("key5", "312312");
    }

    @Test
    void testTemplate15() {
        //操作list 获取listkey 数据
        List<String> key5 = redisTemplate.opsForList().range("key5", 0, -1);
        System.out.println(key5);
    }

    @Test
    void testTemplate16() {
        Long key5 = redisTemplate.opsForList().size("key5");
        System.out.println(key5);
    }

    @Test
    void testTemplate17() {
        //弹出最左边
        redisTemplate.opsForList().leftPop("key5");
    }


    @Test
    void testTemplate5() {
        //操作set
        HashSet<String> strings = new HashSet<>();
        strings.add("6");
        strings.add("48");
        strings.add("7");
        strings.add("4");
        strings.add("4");
        redisTemplate.opsForSet().add("key6", String.valueOf(strings));
    }

    @Test
    void testTemplate52() {
        Boolean key6 = redisTemplate.opsForSet().isMember("key6", "1");
        Set<String> key61 = redisTemplate.opsForSet().members("key6");
        System.out.println(key6);
    }

    @Test
    void testTemplate18() {
        //操作Set 删除
        redisTemplate.opsForSet().pop("key6");
        //随机弹出 2个
        redisTemplate.opsForSet().pop("key6", 2);
    }

    @Test
    void testTemplate19() {
        //操作zSet 长度
        redisTemplate.opsForSet().size("key6");
    }

    @Test
    void testTemplate20() {
        //操作zSet 比不同

        redisTemplate.opsForSet().add("key11","2","5","6");
        redisTemplate.opsForSet().add("key22","1","2","3");

        Set<String> difference = redisTemplate.opsForSet().intersect("key11", "key22");
        Set<String> union = redisTemplate.opsForSet().union("key11", "key22");
        System.out.println(difference);
        System.out.println(union);
    }

    @Test
    void testTemplate6() {
        //操作zSet
        //redisTemplate.opsForZSet().add("key7","xiaoming",80);
        redisTemplate.opsForZSet().add("key7", "xiaoming3", 322);
        redisTemplate.opsForZSet().add("key7", "xiaoming32", 3322);
        redisTemplate.opsForZSet().add("key7", "xiaoming3321", 3232);
    }

    @Test
    void testTemplate29() {
        //操作zSet
        redisTemplate.opsForZSet().rangeByScore("key7", 100, 200);
    }

    @Test
    void testTemplate28() {
        //操作zSet
        String key7 = redisTemplate.opsForZSet().randomMember("key7");
        System.out.println(key7);
    }

    @Test
    void testTemplate7() {
        //使用hash 保存一个对象到
        User user = new User();
        user.setName("小明");
        user.setAge(18);
        user.setId(1L);
        //对象转成字符串
        String userString = JSON.toJSONString(user);
        redisTemplate.opsForValue().set("user-key", userString);
    }
}
