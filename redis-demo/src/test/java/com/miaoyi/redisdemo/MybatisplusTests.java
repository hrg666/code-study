package com.miaoyi.redisdemo;


import com.miaoyi.redisdemo.entity.WxLotteryPrize;
import com.miaoyi.redisdemo.service.WxLotteryPrizeService;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/*@RunWith(SpringRunner.class)
@SpringBootTest(classes = RedisDemoApplication.class)*/
//这里的mapper需要贴 mapper注解
//@SpringBootTest
class MybatisplusTests {

    @Autowired
    private WxLotteryPrizeService lotteryPrizeService;
    @Test
    void contextLoads() {
        WxLotteryPrize wxLotteryPrize = new WxLotteryPrize();
        wxLotteryPrize.setActId("66");
        lotteryPrizeService.save(wxLotteryPrize);


       lotteryPrizeService.getOne(Wrappers.lambdaQuery(WxLotteryPrize.class)
                .eq(WxLotteryPrize::getActId, "66"));

    }


}
