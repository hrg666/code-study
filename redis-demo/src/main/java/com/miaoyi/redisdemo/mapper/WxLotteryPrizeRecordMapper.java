package com.miaoyi.redisdemo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.miaoyi.redisdemo.entity.WxLotteryPrizeRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 中奖记录表 Mapper 接口
 * </p>
 *
 * @author hhh
 * @since 2023-11-21
 */

public interface WxLotteryPrizeRecordMapper extends BaseMapper<WxLotteryPrizeRecord> {

}
