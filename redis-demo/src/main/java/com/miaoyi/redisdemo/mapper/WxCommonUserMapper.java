package com.miaoyi.redisdemo.mapper;


import com.miaoyi.redisdemo.entity.WxCommonUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 活动用户表 Mapper 接口
 * </p>
 *
 * @author hhh
 * @since 2023-11-18
 */
@Mapper
public interface WxCommonUserMapper extends BaseMapper<WxCommonUser> {

}
