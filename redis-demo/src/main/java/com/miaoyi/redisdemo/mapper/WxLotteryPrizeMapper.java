package com.miaoyi.redisdemo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.miaoyi.redisdemo.entity.WxLotteryPrize;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 奖品表 Mapper 接口
 * </p>
 *
 * @author hhh
 * @since 2023-11-17
 */
@Mapper
public interface WxLotteryPrizeMapper extends BaseMapper<WxLotteryPrize> {

}
