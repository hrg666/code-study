package com.miaoyi.redisdemo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.miaoyi.redisdemo.entity.WxLotteryPrizeRecord;

/**
 * <p>
 * 中奖记录表 服务类
 * </p>
 *
 * @author hhh
 * @since 2023-11-21
 */
public interface WxLotteryPrizeRecordService extends IService<WxLotteryPrizeRecord> {

}
