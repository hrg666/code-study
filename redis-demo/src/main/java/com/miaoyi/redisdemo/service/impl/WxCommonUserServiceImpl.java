package com.miaoyi.redisdemo.service.impl;


import com.miaoyi.redisdemo.entity.WxCommonUser;
import com.miaoyi.redisdemo.mapper.WxCommonUserMapper;
import com.miaoyi.redisdemo.service.WxCommonUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 活动用户表 服务实现类
 * </p>
 *
 * @author hhh
 * @since 2023-11-18
 */
@Service
public class WxCommonUserServiceImpl extends ServiceImpl<WxCommonUserMapper, WxCommonUser> implements WxCommonUserService {

}
