package com.miaoyi.redisdemo.service;


import com.miaoyi.redisdemo.entity.WxCommonUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 活动用户表 服务类
 * </p>
 *
 * @author hhh
 * @since 2023-11-18
 */
public interface WxCommonUserService extends IService<WxCommonUser> {

}
