package com.miaoyi.redisdemo.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.miaoyi.redisdemo.entity.WxLotteryPrizeRecord;
import com.miaoyi.redisdemo.mapper.WxLotteryPrizeRecordMapper;
import com.miaoyi.redisdemo.service.WxLotteryPrizeRecordService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 中奖记录表 服务实现类
 * </p>
 *
 * @author hhh
 * @since 2023-11-21
 */
@Service
public class WxLotteryPrizeRecordServiceImpl extends ServiceImpl<WxLotteryPrizeRecordMapper, WxLotteryPrizeRecord> implements WxLotteryPrizeRecordService {

}
