package com.miaoyi.redisdemo.utils;


import constans.R;
import constans.ReturnCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class CommonControllerAdvice {

    @ExceptionHandler(MyBusinessException.class)
    @ResponseBody
    public R handleBusinessException(MyBusinessException ex) {
        log.info("自定义异常MyBusinessException：{[]}：",ex);
        ex.printStackTrace();
        return R.failed(ex.getCode(),ex.getMsg());
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public R handleDefaultException(Exception ex) {
        log.error("出现的异常:{[]}：",ex);
        ex.printStackTrace();
        return R.failed(ReturnCode.EXCEPTION, "系统繁忙");
    }
}
