package com.miaoyi.redisdemo.utils;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MyBusinessException extends RuntimeException {
    private String msg;
    private int code;
    public MyBusinessException(int code, String msg){
        this.code = code;
        this.msg = msg;
    }
}
