package com.miaoyi.redisdemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 中奖记录表
 * </p>
 *
 * @author hhh
 * @since 2023-11-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="WxLotteryPrizeRecord对象", description="中奖记录表")
public class WxLotteryPrizeRecord implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "活动id")
    private String actId;

    @ApiModelProperty(value = "奖品id")
    private Integer prizeId;

    private Integer prizeLevel;

    @ApiModelProperty(value = "奖品名称")
    private String prizeName;

    private Integer prizeType;

    private String prizeImg;

    @ApiModelProperty(value = "兑奖说明")
    private String prizeExplain;

    private String openid;

    private String nickName;

    @ApiModelProperty(value = "状态")
    private Integer state;

    private String phone;

    private String addPhone;

    private String name;

    private String address;

    @ApiModelProperty(value = "券码/链接")
    private String coupon;

    private Date createTime;

    private Date updateTime;

    private String prizeRecordImg;

    private String prizeToast;

    private Date deadTime;

    private String prizeShowImg;

    private Double price;


}
