package com.miaoyi.redisdemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 活动用户表
 * </p>
 *
 * @author hhh
 * @since 2023-11-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="WxCommonUser对象", description="活动用户表")
public class WxCommonUser implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "活动id")
    private String actId;

    @ApiModelProperty(value = "微信id")
    private String openid;

    @ApiModelProperty(value = "微信名")
    private String nickName;

    @ApiModelProperty(value = "头像")
    private String img;

    @ApiModelProperty(value = "电话号码")
    private String phone;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "是否关注0否1是")
    private Integer isAuthorizer;

    @ApiModelProperty(value = "多用字段/复活次数")
    private Integer shareNum;

    private Integer showNum;

    @ApiModelProperty(value = "性别")
    private String sex;

    private String city;

    @ApiModelProperty(value = "运营商")
    private String operator;

    @ApiModelProperty(value = "登记手机")
    private String addPhone;

    @ApiModelProperty(value = "登记姓名")
    private String addName;

    @ApiModelProperty(value = "访问渠道")
    private String cl;

    private Date createTime;

    private Date updateTime;

    private String unionid;

    private String miaoyiOpenid;

    @ApiModelProperty(value = "用户星级")
    private Integer starLevel;

    private String birthMonth;

    private Integer isTelecom;

    private String custNbr;

    private String papers;

    private Integer hasStrategy;

    @ApiModelProperty(value = "积分")
    private String userPoint;

    @ApiModelProperty(value = "年底积分")
    private String deadPoint;

    @ApiModelProperty(value = "氪金得分")
    private Integer goldScore;

    @ApiModelProperty(value = "网龄得分")
    private Integer ageScore;

    @ApiModelProperty(value = "活跃得分")
    private Integer activeScore;

    @ApiModelProperty(value = "信誉得分")
    private Integer reputationScore;

    @ApiModelProperty(value = "使用得分")
    private Integer useScore;

    @ApiModelProperty(value = "抽奖次数")
    private Integer lotteryNum;

    private String qualifiedState;


}
