package com.miaoyi.redisdemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 奖品表
 * </p>
 *
 * @author hhh
 * @since 2023-11-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="WxLotteryPrize对象", description="奖品表")
public class WxLotteryPrize implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String actId;

    @ApiModelProperty(value = "0特等奖1一等奖2二等奖3三等奖 99不中奖")
    private Integer prizeLevel;

    private String prizeName;

    @ApiModelProperty(value = "奖品图片")
    private String prizeImg;

    @ApiModelProperty(value = "奖品总数")
    private Integer prizeTotal;

    @ApiModelProperty(value = "奖品剩余数量")
    private Integer prizeNum;

    @ApiModelProperty(value = "类型 1实体 2虚拟 3卡密 4链接 5banner 6外链 7直充")
    private Integer prizeType;

    @ApiModelProperty(value = "状态")
    private Integer prizeState;

    @ApiModelProperty(value = "排序")
    private Integer prizeSort;

    @ApiModelProperty(value = "概率")
    private String prizeProbability;

    @ApiModelProperty(value = "新用户概率")
    private String prizeProbabilityNew;

    @ApiModelProperty(value = "中奖提示语")
    private String prizeToast;

    @ApiModelProperty(value = "兑奖说明")
    private String prizeExplain;

    @ApiModelProperty(value = "价格")
    private Double price;

    private Date createTime;

    private Date updateTime;

    private String couponCode;

    @ApiModelProperty(value = "抽到的图片")
    private String prizeRecordImg;

    private Integer version;


}
