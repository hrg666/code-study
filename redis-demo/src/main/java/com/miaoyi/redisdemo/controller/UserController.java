package com.miaoyi.redisdemo.controller;

import com.miaoyi.redisdemo.entity.WxLotteryPrize;
import com.miaoyi.redisdemo.service.WxLotteryPrizeService;
import constans.R;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private WxLotteryPrizeService lotteryPrizeService;
    @GetMapping("browse")
    public HashMap<String, Object> browse(Long id) {
        String content ="哈哈哈哈哈哈，这是新闻";
        HashMap<String, Object> map = new HashMap<>();
        String key = "view"+id;
        Long count = redisTemplate.opsForValue().increment(key);
        map.put("content",content);
        map.put("count",count);
        return map;
    }

    @PostMapping("prize")
    @ApiOperation("奖品")
    public R prize(@RequestParam String actId) {
        WxLotteryPrize wxLotteryPrize = new WxLotteryPrize();
        wxLotteryPrize.setActId("66");
        lotteryPrizeService.save(wxLotteryPrize);

        return R.ok();
    }
    @PostMapping("prize2")
    @ApiOperation("本地缓存")
    public R prize2() {

        lotteryPrizeService.saveben();

        return R.ok();
    }

    @PostMapping("prize22")
    @ApiOperation("本地缓存2")
    public R prize22() {

        lotteryPrizeService.saveben2();

        return R.ok();
    }
}
