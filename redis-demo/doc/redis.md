### redis 介绍

Redis是以key-value store存储.
key: 基本上就是字符串
value可以包含:(string)字符串,哈希,(list)链表,(set)集合,(zset)有序集合.这些数据集合都指出push/pop,add/remove及取交集和并集以及更丰富的操作.redis支持各种不同方式排序,为了保证效率,数据都是缓存在内存中.它可以从周期性的把更新的数据写入到磁盘或者把修改操作写入追加的文件中.


redis定位是缓存, 提高数据读写速度, 减轻对数据库存储与访问压力

### 数据类型

String hash list set zset

list和set、zset 一样 zset可以排序

![image-20231116185709782](images/image-20231116185709782.png)  

总结里面存的都是string

### 命令

```java
返回满足的所有键 
keys * (可以模糊查询)
exists 是否存在指定key
expire 设置某个key的过期时间.使用ttl查看剩余时间
persist 取消过期时间
```



### springboot操作

```java
 @Test
void testTemplate() {
    	//操作String存入键值对
   	    redisTemplate.opsForValue().set("xioaming","66");
        
        //把值增加1 默认从1开始
       redisTemplate.opsForValue().increment("huangrui");

        //把值增减1
        redisTemplate.opsForValue().decrement("xioaming");

        //删除键值对
        Boolean xioaming = redisTemplate.delete("xioaming"); 
    
         //存入键值对 失效时间
        redisTemplate.opsForValue().set("age","66",1, TimeUnit.MINUTES);
        redisTemplate.opsForValue().set("sex","1",7, TimeUnit.DAYS);
    	
    	//为已存在的key设置过期时间 如果key本身存在过期时间则重置
     	redisTemplate.expire("age3",3, TimeUnit.MINUTES);
    }
```



##### 操作hash

```java
//put当个
redisTemplate.opsForHash().put("key2","age1","15");

//put多个 里面hashkey不能重复
HashMap<String, Object> map = new HashMap<>();
map.put("name","小明");
map.put("age","18");
redisTemplate.opsForHash().putAll("key2",map);

//put对象
User user = new User();
user.setAge(14);
user.setId(14L);
user.setName("黄瑞国");
String s = JSON.toJSONString(user);
redisTemplate.opsForHash().put("key2",user.getId()+"",s);

//获取
String o = (String) redisTemplate.opsForHash().get("key2", "age");
User user = JSON.parseObject(o, User.class);
System.out.println(user);
System.out.println(o);
```

##### 操作list

```java
redisTemplate.opsForList().leftPushAll("key5","3","4","5");

//修改里面的第一个值
redisTemplate.opsForList().set("key5",0,"8989");
//右加 左加
redisTemplate.opsForList().rightPush("key5","312312");
redisTemplate.opsForList().leftPush("key5","312312");

//操作list 获取listkey 数据
List<String> key5 = redisTemplate.opsForList().range("key5", 0, -1);
System.out.println(key5);

//弹出最左边
redisTemplate.opsForList().leftPop("key5");

//获取数量
Long key5 = redisTemplate.opsForList().size("key5");
System.out.println(key5);
```

##### 操作set

```java
//操作set
HashSet<String> strings = new HashSet<>();
strings.add("6");
strings.add("48");
strings.add("7");
strings.add("4");
strings.add("4");
redisTemplate.opsForSet().add("key6", String.valueOf(strings));
redisTemplate.opsForSet().add("key6", "1","12","121");

//判断是否存在
Boolean key6 = redisTemplate.opsForSet().isMember("key6", "1");
System.out.println(key6);
// 删除
redisTemplate.opsForSet().pop("key6");
//随机弹出 2个
redisTemplate.opsForSet().pop("key6",2);

//操作zSet 长度
redisTemplate.opsForSet().size("key6");

 //操作zSet 比不同  key1的key2的不同 输出key1不同的元素   注意：key得是set的类型要不然报错
Set<String> difference = redisTemplate.opsForSet().difference("key1", "key2");
//去除key1和key2一样的
Set<String> difference = redisTemplate.opsForSet().intersect("key1", "key2");

//取出所有
Set<String> union = redisTemplate.opsForSet().union("ke1", "ke2");

 //获取所有数据
Set<String> key61 = redisTemplate.opsForSet().members("key6");
```

![image-20231116193107179](images/image-20231116193107179.png)



##### 操作zset

```java
redisTemplate.opsForZSet().add("key7", "xiaoming3", 322);
```

### Redisson

```java
<dependency>
	<groupId>org.redisson</groupId>
	<artifactId>redisson</artifactId>
	<version>3.13.6</version>
</dependency>


@Configuration
public class RedissonConfig {

    @Bean
    public RedissonClient redissonClient(){
        // 配置
        Config config = new Config();
        config.useSingleServer().setAddress("redis://192.168.150.101:6379")
            .setPassword("123321");
        // 创建RedissonClient对象
        return Redisson.create(config);
    }
}
```

##### 使用

```java
@Resource
private RedissionClient redissonClient;

@Test
void testRedisson() throws Exception{
    String phone="31231";
    //获取锁(可重入)，指定锁的名称
        RLock lock = redissonClient.getLock("anyLock:"+phone);
    
   //解释：3个参数 1代表可以尝试获取锁 每1秒尝试 10是10秒超时自动释放锁  TimeUnit.SECONDS都是秒
    boolean isLock = lock.tryLock(1,10,TimeUnit.SECONDS);
    //不带参数 说明获取不到锁就不去尝试  获取不到就算了的意思
     boolean isLock2 = lock.tryLock();
    //判断获取锁成功
    if(isLock){
        try{
            System.out.println("执行业务");          
        }finally{
            //释放锁
            lock.unlock();
        }
    }
    
}
```

##### 总结

```java
RLock lock = redissonClient.getLock("anyLock:" + "13536477084"); //用手机 锁的粒度低


boolean isLock = lock.tryLock();//排队只有一条线程进来

//1代表去尝试获取锁  每1秒去尝试 10秒后还没有执行完业务去解掉锁
//写上过期时间 可能出现并发问题 业务时间长的话
boolean isLock = lock.tryLock(1,10, TimeUnit.SECONDS);

lock.lock();//会一直阻塞等待
lock.lock(4,TimeUnit.SECONDS);//一直阻塞等待 ，4秒后业务时间超过了去解锁

不加参数会续约锁的时间 推荐使用 不会出现并发问题
```

