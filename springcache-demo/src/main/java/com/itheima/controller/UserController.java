package com.itheima.controller;

import com.itheima.entity.User;
import com.itheima.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserMapper userMapper;


    //toDo @CachePut   新增缓存进去
    //     @Cacheable   查询出来的值先存入redis中 之后再次执行就从redis取

    //#user取参数里的 返回值 user.id
    @PostMapping
    @CachePut(value = "userCache", key = "#user.id")//key的生成：userCache::xx
    //@CachePut(value = "userCache", key = "#result.id")//key的生成：userCache::1
    //@CachePut(value = "userCache", key = "#p0.id")//key的生成：userCache::1
    //@CachePut(value = "userCache", key = "#root.args[0].id")//key的生成：userCache::1
    public User save(@RequestBody User user){
        userMapper.insert(user);
        return user;
    }

    @GetMapping
    @Cacheable(value = "userCache", key = "#id")//key的生成：userCache::xx  查询不到会缓存null
    public User getById(Long id){
        User user = userMapper.getById(id);
        return user;
    }

    @DeleteMapping
    @CacheEvict(cacheNames = "userCache",key = "#id")//删除某个key对应的缓存数据
    public void deleteById(Long id){
        userMapper.deleteById(id);
    }

	@DeleteMapping("/delAll")
    @CacheEvict(cacheNames = "userCache",allEntries = true)//删除userCache下所有的缓存数据
    public void deleteAll(){
        userMapper.deleteAll();
    }



}
