package cn.wolfcode;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.query.EmployeeQueryObject;
import cn.wolfcode.service.IEmployeeService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SerivceTest {


    //todo 分页查询

    @Test
    public void selectPageQuery(){
        EmployeeQueryObject qo = new EmployeeQueryObject();
        qo.setCurrentPage(2);
        qo.setPageSize(5);


        LambdaQueryWrapper<Employee> query = new LambdaQueryWrapper<>();
        query.orderByDesc(Employee::getAge);
        //Page<Object> age = new Page<>(qo.getCurrentPage(), qo.getPageSize()).addOrder(OrderItem.asc("age"));
        Page<Employee> page = employeeService.page(new Page<>(qo.getCurrentPage(), qo.getPageSize()), query);





        System.out.println("当前页：" + page.getCurrent());
        System.out.println("总页数：" + page.getPages());
        System.out.println("每页显示条数：" + page.getSize());
        System.out.println("总记录数：" + page.getTotal());
        System.out.println("当前页显示记录：" + page.getRecords());
    }



    @Autowired
    private IEmployeeService employeeService;

    @Test
    public void test(){
       employeeService.getById(2L);
    }

    @Test
    public void selectPage(){
        EmployeeQueryObject qo = new EmployeeQueryObject();
        qo.setCurrentPage(2);
        qo.setPageSize(5);
        Page<Employee> page = employeeService.queryPage(qo);
        System.out.println("当前页：" + page.getCurrent());
        System.out.println("总页数：" + page.getPages());
        System.out.println("每页显示条数：" + page.getSize());
        System.out.println("总记录数：" + page.getTotal());
        System.out.println("当前页显示记录：" + page.getRecords());
    }


}

