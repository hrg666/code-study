package cn.wolfcode;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.mapper.EmployeeMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;
import java.util.Date;


@SpringBootTest
public class CRUDTest {
    @Autowired
    private EmployeeMapper employeeMapper;

    @Test
    public void testSave(){
        Employee employee = new Employee();
        employee.setAdmin(1);
        employee.setAge(18);
        employee.setDeptId(1L);
        employee.setEmail("dafei@wolfcode");
        employee.setName("dafei");
        employee.setPassword("111");
        employeeMapper.insert(employee);
    }
    @Test
    public void testDelete(){
        employeeMapper.deleteById(20L);
    }
    @Test
    public void testUpdate(){
        Employee employee = new Employee();
        employee.setId(1327139013313564674L); //设置id
        employee.setAdmin(1);
        employee.setDeptId(1L);
        employee.setEmail("dafei@wolfcode");
        employee.setName("dafei");
        employee.setPassword("222");
        employeeMapper.updateById(employee); //这个方法会设置所有

    }
    @Test
    public void testGet(){
        employeeMapper.selectById(1327139013313564674L);
    }
    @Test
    public void testSelect(){
        employeeMapper.selectList(null);//不加条件 传入个null
    }
    @Test
    public void testSelect2(){
        Employee employee = employeeMapper.selectById(1);
        long time = employee.getTime().getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1); //减少一天 从周一开始
        int currentWeek = calendar.get(Calendar.WEEK_OF_YEAR);//本年的第几周
        calendar.setTime(new Date(time));//设置传过来的时间
        calendar.add(Calendar.DATE, -1); //减少一天
        int paramWeek = calendar.get(Calendar.WEEK_OF_YEAR);
        System.out.println( paramWeek==currentWeek);

    }
}
