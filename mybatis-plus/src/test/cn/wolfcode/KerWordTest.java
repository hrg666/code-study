package cn.wolfcode;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.mapper.EmployeeMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

@SpringBootTest
public class KerWordTest {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Test
    public void testSelect() {
        //需求：查询所有员工， 返回员工name， age列
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("name","age");
        employeeMapper.selectList(queryWrapper);
    }

    @Test
    public void testSelect2() {
        //需求：查询所有员工， 返回员工以a字母开头的列
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(Employee.class,pro->pro.getProperty().startsWith("a"));
        employeeMapper.selectList(queryWrapper);
    }

    @Test
    public void testSelect3() {
        //需求：查询所有员工信息按age正序排， 如果age一样， 按id正序排
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
       queryWrapper.orderByAsc("age","id"); //正序
        employeeMapper.selectList(queryWrapper);
    }

    @Test
    public void testSelect4() {
        //需求：查询所有员工信息按age正序排， 如果age一样， 按id正序排
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("age","id"); //降序
        employeeMapper.selectList(queryWrapper);
    }

    @Test
    public void testSelect5() {
        //需求：查询所有员工信息按age正序排， 如果age一样， 按id正序排
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        //第二参数否是正序
        queryWrapper.orderBy(true,true,"age","id");// 等价于：orderByAsc("age", "id");
        employeeMapper.selectList(queryWrapper);
    }


    @Test
    public void testSelect6() {
        //需求：查询所有员工信息按age正序排， 如果age一样， 按id倒序排
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("age");
        queryWrapper.orderByDesc("id");
        employeeMapper.selectList(queryWrapper);
    }

    @Test
    public void testSelect7() {
        //需求： 以部门id进行分组查询，查每个部门员工个数
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        queryWrapper.groupBy("dept_id");
        queryWrapper.select("dept_id","count(1)");
        employeeMapper.selectList(queryWrapper);
    }

    @Test
    public void testSelect8() {
       // 需求： 以部门id进行分组查询，查每个部门员工个数， 将大于3人的部门过滤出来
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        queryWrapper.groupBy("dept_id");
        queryWrapper.select("dept_id","count(1) count"); //1代表常量 都可以查到
        queryWrapper.having("count>3");
        employeeMapper.selectList(queryWrapper);
    }

    @Test
    public void testSelect9() {
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        Map<String, Object> map = new HashMap<>();
        map.put("name", "dafei");
        map.put("age", 78);
        wrapper.allEq(map);
        employeeMapper.selectList(wrapper);
    }

    @Test
    public void testQuery11(){
        //需求：查询name=dafei， age=18的员工信息
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        Map<String, Object> map = new HashMap<>();
        map.put("name","dafei");
        map.put("age",78);
        wrapper.allEq(map);
        employeeMapper.selectList(wrapper);

    }
    @Test
    public void testSelect10() {
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        wrapper.ne("name", "dafei");
        employeeMapper.selectList(wrapper);
    }

    @Test
    public void testSelect11() {
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        wrapper.gt("age", 18); //gt 大于 ge大于等于  lt 小于 le 小于等于
        employeeMapper.selectList(wrapper);
    }

    @Test
    public void testSelect12() {
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        wrapper.isNotNull("age"); //isnull
        employeeMapper.selectList(wrapper);
    }

    @Test
    public void testSelect13() {
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        wrapper.in("age",18,25);
        employeeMapper.selectList(wrapper);
    }
    @Test
    public void testSelect14() {
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        wrapper.inSql("age","12,25"); //会自动给你加()  age IN (12,25)
        employeeMapper.selectList(wrapper);
    }
    @Test
    public void testSelect15() {
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        wrapper.exists("select id from employee where age>58");
        employeeMapper.selectList(wrapper);
    }

    @Test
    public void testSelect16() {
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        wrapper.like("name","fei");  //noLike 不是的意思
        employeeMapper.selectList(wrapper);
    }
    @Test
    public void testSelect17() {
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        wrapper.likeLeft("name","fei");  //likeRight 不是的意思
        employeeMapper.selectList(wrapper);
    }

    @Test
    public void testSelect18() {
        //查询age = 18 或者 name=dafei 或者 id =1 的用户
        //select * from employee where age=18 or name="dafei" or id=1
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        //or()的 使用在需要的条件下 就加在中间
        wrapper.eq("age",18).or().eq("name","dafei").or().eq("id",1);
        employeeMapper.selectList(wrapper);
    }

    @Test
    public void testSelect19() {
        //需求：查询name含有fei字样的并且 年龄在小于18或者大于30的用户
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
       wrapper.eq("name","dafei").and(new Consumer<QueryWrapper<Employee>>() {
           @Override
           public void accept(QueryWrapper<Employee> employeeQueryWrapper) {
               employeeQueryWrapper.lt("age",30).or().gt("age",30);
           }
       });
       employeeMapper.selectList(wrapper);
    }
}
