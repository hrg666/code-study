package cn.wolfcode;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.mapper.EmployeeMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
public class MapperTest {

    @Autowired
    private EmployeeMapper employeeMapper;


    @Test
    public void testSave() {
        Employee employee = new Employee();
        employee.setAdmin(1);
        employee.setDeptId(1L);
        employee.setEmail("12@");
        employee.setName("雄安民");
        employee.setPassword("123");
        employee.setAge(18);
        employeeMapper.insert(employee);
    }

    @Test
    public void testUpdateById() {
        //  需求： 将id=1用户名字修改为dafei
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName("dafei");
        employeeMapper.updateById(employee); //会更新所有
    }

    @Test
    public void testUpdateById2() {
        // 改进：
        // 1：先查
        //2：替换
        //3：更新
        // 需求： 将id=1用户名字修改为dafei
        Employee employee = employeeMapper.selectById(1L);
        employee.setName("dafei");
        employeeMapper.updateById(employee);
    }


    @Test
    public void testUpdate3() {
        //需求：更新name=dafei员工年龄为18岁
        Employee employee = new Employee();
        employee.setAge(18);
        //update语句 where包装
        UpdateWrapper<Employee> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("name", "dafei");  //eq是等于  key是列 value是值
        employeeMapper.update(employee, updateWrapper);
    }

    @Test
    public void testUpdate4() {
        //需求：更新name=dafei员工年龄为19岁  升级版
        Employee employee = new Employee();
        //update语句 where包装
        UpdateWrapper<Employee> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("name", "dafei")  //eq是等于  key是列 value是值
                .set("age", 19); //设置值  key是列 value是值
        employeeMapper.update(employee, updateWrapper);
    }

    @Test
    public void testUpdate5() {
        //需求：更新name=dafei，并且password=1111的员工年龄为18岁
        Employee employee = new Employee();
        //update语句 where包装
        UpdateWrapper<Employee> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("name", "dafei")
                .eq("password", 1111) //多个eq默认是and拼接
                .set("age", 18);
        employeeMapper.update(employee, updateWrapper);
    }

    @Test
    public void testDeleteById() {
        employeeMapper.deleteById(19L);
    }

    @Test
    public void testDeleteBatchIds() {
        //删除多个
        employeeMapper.deleteBatchIds(Arrays.asList(1, 2, 3));
    }

    @Test
    public void testDeleteByMap() {
        //需求：删除name=dafei并且age=18的员工信息
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", "dafei");
        map.put("age", 18);
        employeeMapper.deleteByMap(map);
    }

    @Test
    public void testDelete() {
        //需求：删除name=dafei并且age=18的员工信息
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", "dafei").eq("age", 18);
        employeeMapper.delete(queryWrapper);
    }

    @Test
    public void testSelectById() {
        //查询id为2的员工
        System.out.println(employeeMapper.selectById(2L));
    }

    @Test
    public void testSelectBatchIds() {
        // 需求：查询id=1，id=2的员工信息
        employeeMapper.selectBatchIds(Arrays.asList(2L, 3L, 4L));
    }

    @Test
    public void testSelectByMap() {
        //需求： 查询name=dafei , age=18的员工信息
        HashMap<String, Object> map = new HashMap();
        map.put("name", "dafei");
        map.put("age", "18");
        employeeMapper.selectByMap(map);
    }

    @Test
    public void selectList() {
        //查询满所有员工
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        List<Employee> employees = employeeMapper.selectList(wrapper);
        employees.forEach(System.out::println);
        //两个方法一样 当没有条件时 可以传入一个null
        List<Employee> employees2 = employeeMapper.selectList(null);
        employees2.forEach(System.out::println);
    }

    @Test
    public void selectTest5() {
        //需求 查询满所有员工个数
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        Integer count = employeeMapper.selectCount(queryWrapper);
        System.out.println(count);
    }

    @Test
    public void testSelectTest6() {
        //需求 查询满足条件dept_id=5 的所有员工个数
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        //eq 是条件  key是列名 value是值
        queryWrapper.eq("dept_id", 5);//根据dept_id=5 查询
        Integer count = employeeMapper.selectCount(queryWrapper);
        System.out.println(count);
    }

    @Test
    public void testSelectMaps() {
        //查询满足条件的所有的员工信息
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        List<Map<String, Object>> list = employeeMapper.selectMaps(queryWrapper);
        for (Map<String, Object> map : list) {
            Set<String> keys = map.keySet();
            for (String s : keys) {
                System.out.println("key:" + s + "  value:" + map.get(s));
            }
        }
    }

    //需求：查询第二页员工数据， 每页显示3条， （分页返回的数据是实体对象） 需要配置分页插件
    @Test
    public void testSelectPage() {
        //参数1：当前页， 参数2：每页显示条数
        Page page = new Page(2, 6);
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        //分页需要两个参数对象
        employeeMapper.selectPage(page, queryWrapper);
    }

    // 需求：将id=1的员工age改为18， 如果传入uname变量值不等于null或者“”，修改为员工name为uname变量值
    @Test
    public void testUpdate() {
        UpdateWrapper<Employee> updateWrapper = new UpdateWrapper<>();
        Employee employee = new Employee();
        String username = "dafei";
        updateWrapper.eq("id", 2L)
                .set("age", 18)
                .set(username != null, "name", username); //开关按钮 第一个参数为true才执行 false不执行
        employeeMapper.update(employee, updateWrapper);
    }

    @Test
    public void testUpdate2() {
        //set片段
        UpdateWrapper<Employee> updateWrapper = new UpdateWrapper<>();
        Employee employee = new Employee();
        updateWrapper.eq("id", 2L)
                .setSql("age=78");
        employeeMapper.update(employee, updateWrapper);
    }

    //last语句
    @Test
    public void test() {
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        queryWrapper.last("limit 1");
        employeeMapper.selectOne(queryWrapper);
    }

    @Test
    public void test1() {
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(Employee::getAge, 78);
        employeeMapper.selectList(queryWrapper);
    }

    @Test
    public void test2() {
        LambdaUpdateWrapper<Employee> updateWrapper = new LambdaUpdateWrapper();
        Employee employee = new Employee();
        updateWrapper.eq(Employee::getAge, 78).set(Employee::getName,"dafei1");
        //updateWrapper.eq(Employee::getAge, 78).set(x->x.getName(),"dafei1");
       employeeMapper.update(employee,updateWrapper);
    }

    @Test
    public void testUpdate7(){
        //需求：将id=1的用户name改为dafei
        LambdaUpdateWrapper<Employee> updateWrapper = new LambdaUpdateWrapper();
        Employee employee = new Employee();
        updateWrapper.eq(Employee::getId,1L).set(Employee::getName,"dafei");
        employeeMapper.update(employee,updateWrapper);
    }

    //需求： 查询满足条件的所有的员工,返回排在第一的列所有数据， 没特别指定， 一般返回时id
    @Test
    public void test8(){
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("name");
        employeeMapper.selectList(queryWrapper);
    }

    //工具类
    @Test
    public void test9(){
        QueryWrapper<Employee> queryWrapper = Wrappers.query();
        queryWrapper.eq("name",18);
        employeeMapper.selectList(queryWrapper);
    }

    @Test
    public void test10(){
        UpdateWrapper<Employee> update = Wrappers.update();
        Employee employee = new Employee();
        update.eq("id",18).set("name","小明");
        employeeMapper.update(employee,update);

    }

    @Test
    public void test11(){
      /*  QueryWrapper<Employee> query = Wrappers.query();
        query.ne("dept_id",3)
                .("name", "Peter");*/
        UpdateWrapper<Employee> userUpdateWrapper = new UpdateWrapper<>();
        userUpdateWrapper

                .set("name", "dafei");




    }

}
