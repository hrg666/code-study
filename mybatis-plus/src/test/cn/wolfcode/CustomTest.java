package cn.wolfcode;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.mapper.EmployeeMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CustomTest {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Test
    public void test(){
        List<Employee> employee = employeeMapper.selectList2();
        System.out.println(employee);
    }
    @Test
    public void test2(){
         employeeMapper.listByXmlJoin();
    }


}

