package cn.wolfcode.controller;


import cn.wolfcode.mp.dto.UserDto;
import cn.wolfcode.mp.hadle.R;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.HashMap;

@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("browse")
    public HashMap<String, Object> browse(Long id) {
        String content ="哈哈哈哈哈哈，这是新闻";
        HashMap<String, Object> map = new HashMap<>();
        String key = "view"+id;

        map.put("content",content);

        return map;
    }

    @PostMapping("/browse1")
    public R browse(@Valid @RequestBody  UserDto dto) {
        String content ="哈哈哈哈哈哈，这是新闻";
        HashMap<String, Object> map = new HashMap<>();


        map.put("content",content);
        map.put("dto",dto);


        return R.ok();
    }

    @GetMapping("/validate1")
    @ResponseBody
    public String validate1(
            @Size(min = 1,max = 10,message = "姓名长度必须为1到10")@RequestParam("name") String name,
            @Min(value = 10,message = "年龄最小为10")@Max(value = 100,message = "年龄最大为100") @RequestParam("age") Integer age) {
        return "validate1";
    }


}
