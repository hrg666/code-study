package cn.wolfcode.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@Getter@Setter
@TableName("department")
public class Department {
    private Long id;
    private String name;
    private String sn;
}
