package cn.wolfcode.mapper;

import cn.wolfcode.domain.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/*
*   BaseMapper:Mybatis-plus 中的增强的一个Mapper接口
*   需要指定泛型 泛型是需要操作的实体对象
*       继承父接口BaseMapper有17个方法可供增删改查操作
*      ① 执行的sql语句是怎么来的
*       表名就是对应的实体类Employee --->employee
*           表名的生成，就是获取到类名的简单名称 首字母小写
*           获取简单名称  表名--> BaseMapper 中的泛型来的
*       二：
*           字段名
*               首先获取泛型找到对应的类
*               根据获取到的所有的属性 id name password。。。
*               把属性名字转换为列名
* */
public interface EmployeeMapper extends BaseMapper<Employee> {

    List<Employee> selectList2();
    List<Employee> listByXmlJoin();
}
