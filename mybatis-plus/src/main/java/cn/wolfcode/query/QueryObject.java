package cn.wolfcode.query;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter@Getter@NoArgsConstructor
public class QueryObject {
    private int pageSize=10;    //当前页多少条数据
    private int currentPage=1;  //当前页
    private String keyWord;     //关键字搜索
}
