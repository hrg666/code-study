package cn.wolfcode.service;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.query.EmployeeQueryObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
//自定义服务接口集成IService接口
public interface IEmployeeService extends IService<Employee> {
    //分页 参数一般当前页 当前页总数 关键字搜索 用对象封装一下
    Page<Employee> queryPage(EmployeeQueryObject qo);

}
