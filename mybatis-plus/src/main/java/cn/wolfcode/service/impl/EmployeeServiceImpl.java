package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.mapper.EmployeeMapper;
import cn.wolfcode.query.EmployeeQueryObject;
import cn.wolfcode.service.IEmployeeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
    /*注意ServiceImpl实现类泛型：
        泛型1：实体类的mapper接口
        泛型2：实体类
    */
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee>implements IEmployeeService {
    //分页实现
    @Override
    public Page<Employee> queryPage(EmployeeQueryObject qo) {
        //1. 创建一个page对象
        Page<Employee> page = new Page<>(qo.getCurrentPage(), qo.getPageSize());
        //创建一个查询对象的构造器 加条件 可以不需要
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        return super.page(page, queryWrapper); //返回的就是Page
    }
}
