package cn.wolfcode.mp.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class UserDto implements Serializable {

    //@NotBlank(message = "品牌名必须非空")
    @Size(max = 200, min = 5, message = "题目长度为5-200")
    //@NotNull(message = "不能为空")
    //private Integer age; //int类型有的捕获不到异常
    private String age;
}
