package cn.wolfcode.mp.hadle;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.UnexpectedTypeException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
//@RestControllerAdvice(basePackages = "cn.wolfcode.controller")
@RestControllerAdvice
public class ValidationExceptionHandler {

   /* @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public R handleVaildException(MethodArgumentNotValidException e){

        log.error("数据校验出现问题：{}，异常类型：{}",e.getMessage(),e.getClass());
        BindingResult bindingResult = e.getBindingResult();
        StringBuffer stringBuffer = new StringBuffer();
        bindingResult.getFieldErrors().forEach(item ->{
            //获取错误信息
            String message = item.getDefaultMessage();
            //获取错误的属性名字
            String field = item.getField();
            stringBuffer.append(field + ":" + message + " ");
        });
        return R.failed(400, stringBuffer + "");

    }*/

    @ExceptionHandler(value = Exception.class)
    public R handleException(Exception e){

        log.error("错误",e);
        return R.failed(400, "系统异常");
    }

    //一般参数校验绑定异常处理
    @ExceptionHandler(BindException.class)
    public Object bindException1(BindException e) {

        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        //多个错误，取第一个
        FieldError error = fieldErrors.get(0);
        String msg = error.getDefaultMessage();
   ;
        return R.failed(msg);
    }

    //JSON参数校验绑定异常处理
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Object bindException2(MethodArgumentNotValidException e) {

        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        //多个错误，取第一个
        FieldError error = fieldErrors.get(0);
        String msg = error.getDefaultMessage();
        return R.failed(msg);
    }

   /* @ExceptionHandler(UnexpectedTypeException.class)
    public Object bindException2(UnexpectedTypeException e) {
        StackTraceElement[] stackTrace = e.getStackTrace();
        String localizedMessage = e.getLocalizedMessage();
        //多个错误，取第一个
      *//*  FieldError error = fieldErrors.get(0);
        String msg = error.getDefaultMessage();*//*
        System.out.println(e.getCause());
        System.out.println(e.getLocalizedMessage());
        System.out.println(e.getMessage());
        System.out.println(e.getSuppressed());
        return R.failed(localizedMessage);
    }*/

    @ExceptionHandler(value = Throwable.class)
    public R handleVaildOtheException(Throwable e){
        return R.failed(401,"未知异常");

    }

}
