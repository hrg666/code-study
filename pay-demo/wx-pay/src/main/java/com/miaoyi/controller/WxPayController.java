package com.miaoyi.controller;


import com.miaoyi.service.WeixinService;
import com.miaoyi.utils.ConstantPropertiesUtils;
import com.miaoyi.utils.R;
import jdk.internal.org.objectweb.asm.Handle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/pay")
public class WxPayController {

    @Autowired
    private WeixinService weixinPayService;

    //生成二维码
    @GetMapping("/createNative/{orderId}")
    public R createNative(
            @PathVariable("orderId") Long orderId) {
        Map<String,Object> map =  weixinPayService.createNative(orderId);
        return R.ok().data(map);
    }

    @GetMapping("test1")
    public HashMap test1() {
        HashMap<Object, Object> map = new HashMap<>();
        map.put("!",ConstantPropertiesUtils.PARTNERKEY);
        return map;
    }


}
