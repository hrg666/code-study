package com.miaoyi.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.wxpay.sdk.WXPayConstants;
import com.github.wxpay.sdk.WXPayUtil;
import com.miaoyi.service.WeixinService;
import com.miaoyi.utils.ConstantPropertiesUtils;
import com.miaoyi.utils.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Service
public class WeixinServiceImpl implements WeixinService {

    //生成二维码
    @Override
    public Map<String, Object> createNative(Long orderId) {
        try {

            //3封装调用微信接口参数
            Map paramMap = new HashMap();
            paramMap.put("appid", ConstantPropertiesUtils.APPID);
            paramMap.put("mch_id", ConstantPropertiesUtils.PARTNER);
            paramMap.put("nonce_str", WXPayUtil.generateNonceStr());
            //String body = orderInfo.getReserveDate() + "就诊"+ orderInfo.getDepname();
            paramMap.put("body", "我是产品信息");
            String outTradeNo = System.currentTimeMillis() + ""+ new Random().nextInt(100);

            paramMap.put("out_trade_no", outTradeNo);
            //paramMap.put("total_fee", order.getAmount().multiply(new BigDecimal("100")).longValue()+"");
            paramMap.put("total_fee", "1");//为了测试
            paramMap.put("spbill_create_ip", "127.0.0.1");
            paramMap.put("notify_url", "http://guli.shop/api/order/weixinPay/weixinNotify");
            paramMap.put("trade_type", "NATIVE");
            //4创建客户端（指定url）
            HttpClient client = new HttpClient("https://api.mch.weixin.qq.com/pay/unifiedorder");
            //5存入请求参数（map=>xml）
            client.setXmlParam(WXPayUtil.generateSignedXml(paramMap,ConstantPropertiesUtils.PARTNERKEY));
            //6发送请求
            client.setHttps(true);
            client.post();
            //7拿到响应，转化类型（xml=>map）
            String result = client.getContent();
            System.out.println("result = " + result);
            Map<String, String> resultMap = WXPayUtil.xmlToMap(result);
            //8封装数据返回
            Map map = new HashMap<>();
            map.put("orderId", orderId);
            map.put("totalFee", 1);
            map.put("resultCode", resultMap.get("result_code"));
            map.put("codeUrl", resultMap.get("code_url"));
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }
  /*  //调用微信查询接口，获得结果
    @Override
    public Map<String, String> queryPayStatus(Long orderId, Integer paymentType) {
        try {
            //1查询订单信息
            OrderInfo orderInfo = orderService.getById(orderId);
            //2封装查询条件
            Map paramMap = new HashMap<>();
            paramMap.put("appid", ConstantPropertiesUtils.APPID);
            paramMap.put("mch_id", ConstantPropertiesUtils.PARTNER);
            paramMap.put("out_trade_no", orderInfo.getOutTradeNo());
            paramMap.put("nonce_str", WXPayUtil.generateNonceStr());
            //3创建客户端，存入参数
            HttpClient client =
                    new HttpClient("https://api.mch.weixin.qq.com/pay/orderquery");
            client.setXmlParam(WXPayUtil.generateSignedXml(
                    paramMap,ConstantPropertiesUtils.PARTNERKEY));
            //4发送请求
            client.setHttps(true);
            client.post();
            //5获取响应，转型，返回
            String result = client.getContent();
            System.out.println("查询支付result = " + result);
            Map<String, String> resultMap = WXPayUtil.xmlToMap(result);
            return resultMap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

    //退款
    @Override
    public Boolean refund(Long orderId) {
        try {
            //1根据参数查询交易记录
            PaymentInfo paymentInfo =
                    paymentService.getPaymentInfo(orderId, PaymentTypeEnum.WEIXIN.getStatus());
            if(paymentInfo==null){
                throw new YyghException(20001,"交易记录有误");
            }
            //2根据交易记录生成退款记录、判断是否已退款
            RefundInfo refundInfo = refundInfoService.saveRefundInfo(paymentInfo);
            if(refundInfo.getRefundStatus().intValue()
                    == RefundStatusEnum.REFUND.getStatus()){
                return true;
            }

            //3封装微信退款参数
            Map<String, String> paramMap = new HashMap<>(8);
            paramMap.put("appid",ConstantPropertiesUtils.APPID);       //公众账号ID
            paramMap.put("mch_id",ConstantPropertiesUtils.PARTNER);   //商户编号
            paramMap.put("nonce_str",WXPayUtil.generateNonceStr());
            paramMap.put("transaction_id",paymentInfo.getTradeNo()); //微信订单号
            paramMap.put("out_trade_no",paymentInfo.getOutTradeNo()); //商户订单编号
            paramMap.put("out_refund_no","tk"+paymentInfo.getOutTradeNo()); //商户退款单号
            //       paramMap.put("total_fee",paymentInfoQuery.getTotalAmount().multiply(new BigDecimal("100")).longValue()+"");
            //       paramMap.put("refund_fee",paymentInfoQuery.getTotalAmount().multiply(new BigDecimal("100")).longValue()+"");
            paramMap.put("total_fee","1");
            paramMap.put("refund_fee","1");
            String paramXml = WXPayUtil.generateSignedXml(paramMap,ConstantPropertiesUtils.PARTNERKEY);
            //4创建客户端
            HttpClient client =
                    new HttpClient("https://api.mch.weixin.qq.com/secapi/pay/refund");
            //5客户端存入参数、开启读取证书
            client.setXmlParam(paramXml);
            client.setHttps(true);
            client.setCert(true);
            client.setCertPassword(ConstantPropertiesUtils.PARTNER);

            //6发送请求
            client.post();
            //7拿响应，判断是否成功，成功后更新退款记录返回结果
            String result = client.getContent();
            System.out.println("退款返回result = " + result);
            Map<String, String> resultMap = WXPayUtil.xmlToMap(result);
            if(null != resultMap && WXPayConstants.SUCCESS.equalsIgnoreCase(resultMap.get("result_code"))){
                refundInfo.setCallbackTime(new Date());
                refundInfo.setTradeNo(resultMap.get("refund_id"));
                refundInfo.setRefundStatus(RefundStatusEnum.REFUND.getStatus());
                refundInfo.setCallbackContent(JSONObject.toJSONString(resultMap));
                refundInfoService.updateById(refundInfo);
                return true;

            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }*/
}
