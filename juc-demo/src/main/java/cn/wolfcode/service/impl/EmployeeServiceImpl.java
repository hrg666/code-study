package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Employee1;
import cn.wolfcode.mapper.EmployeeMapper;
import cn.wolfcode.service.IEmployeeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
    /*注意ServiceImpl实现类泛型：
        泛型1：实体类的mapper接口
        泛型2：实体类
    */
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee1>implements IEmployeeService {

}
