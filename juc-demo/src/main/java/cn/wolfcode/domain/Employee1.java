package cn.wolfcode.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Setter
@Getter
@ToString
@TableName("employee") //可以手动指定表名
public class Employee1 {
    @TableId(type = IdType.AUTO) //使用主键自增   不使用默认是雪花算法生成的 一堆数字
    private Long id;
    private String name;
    @TableField("password")  //指定列名
    private String password;
    private String email;
    private Integer age;
    private Integer admin;
    private Long deptId;//驼峰命名时 为自动变成 dept_id 列名
    private Date time;

    @TableField(exist = false) //不存在该列名
    private Department dept;
}
