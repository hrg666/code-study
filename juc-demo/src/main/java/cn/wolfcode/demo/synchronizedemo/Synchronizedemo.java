package cn.wolfcode.demo.synchronizedemo;

import java.util.concurrent.TimeUnit;

public class Synchronizedemo {

    //todo 锁
    /**
     * 悲观锁 乐观锁
     * 能用对象锁 就不要用类锁
     * ● 作用于实例方法，当前实例加锁，进入同步代码块前要获得当前实例的锁；
     * ● 作用于代码块，对括号里配置的对象加锁
     * ● 作用于静态方法，当前类加锁，进去同步代码前要获得当前类对象的锁
     *
     * 公平锁 与非公平锁
     * Lock lock = new  ReentrantLock(false)---表示非公平锁，后来的也可能先获得锁，默认为非公平锁
     *
     *
     */


    public static void main(String[] args) {
        //8锁情况
        /**
         * 现象描述：
         * 1 标准访问ab两个线程，请问先打印邮件还是短信？ --------先邮件，后短信  共用一个对象锁
         * 2. sendEmail钟加入暂停3秒钟，请问先打印邮件还是短信？---------先邮件，后短信  共用一个对象锁
         * 3. 添加一个普通的hello方法，请问先打印普通方法还是邮件？ --------先hello，再邮件
         * 4. 有两部手机，请问先打印邮件还是短信？ ----先短信后邮件  资源没有争抢，不是同一个对象锁
         * 5. 有两个静态同步方法，一步手机， 请问先打印邮件还是短信？---------先邮件后短信  共用一个类锁
         * 6. 有两个静态同步方法，两部手机， 请问先打印邮件还是短信？ ----------先邮件后短信 共用一个类锁
         * 7. 有一个静态同步方法 一个普通同步方法，请问先打印邮件还是短信？ ---------先短信后邮件   一个用类锁一个用对象锁
         * 8. 有一个静态同步方法，一个普通同步方法，两部手机，请问先打印邮件还是短信？ -------先短信后邮件 一个类锁一个对象锁
         */
        Phone phone = new Phone();
        new Thread(() -> {
            phone.sendEmail();
        }, "a").start();
        try { TimeUnit.MILLISECONDS.sleep(200); } catch (InterruptedException e) { e.printStackTrace(); }
        new Thread(() -> {
            phone.sendSMS();
        }, "b").start();

    }
}


class Phone {
    public synchronized void sendEmail() {
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("------sendEmail");
    }

    public synchronized void sendSMS() {
        System.out.println("------sendSMS");
    }

    public void hello() {
        System.out.println("------hello");
    }
}
