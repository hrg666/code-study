package cn.wolfcode.controller;


import cn.wolfcode.domain.Employee1;
import cn.wolfcode.mapper.EmployeeMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.concurrent.*;

@RestController
@RequestMapping("/thread")
public class ThreadPoolController {

    final static ExecutorService executorService = Executors.newFixedThreadPool(5);

    @Autowired
    private EmployeeMapper employeeMapper;


    @GetMapping("/id1")
    public String id1() {

        ExecutorService executorService = Executors.newFixedThreadPool(5);//创建固定线程数5

        executorService.execute(() -> {
            System.out.println(Thread.currentThread().getName() + "来人了，出来接客了。。。。。");
        });
        executorService.submit(() -> {
            System.out.println(Thread.currentThread().getName() + "来人了，出来接客了。。。。。");
        });

        return "1";
    }

    //new ThreadPoolExecutor 创建线程
    public static void main(String[] args) {
        ExecutorService executorService = new ThreadPoolExecutor(
                2, //核心
                5,//最大
                2, //空闲存活时间
                TimeUnit.SECONDS,//时间单位
                new ArrayBlockingQueue<>(3),//阻塞队列
                Executors.defaultThreadFactory(), //线程工厂
                //new ThreadPoolExecutor.AbortPolicy()//默认拒绝策略 直接报错
                new ThreadPoolExecutor.CallerRunsPolicy()//CallerRunsPolicy 将被拒绝的任务添加到线程池正在运行线程中去执行 这个不会丢弃
                //new ThreadPoolExecutor.DiscardPolicy()//DiscardPolicy 丢弃任务，不抛出异常
                //new ThreadPoolExecutor.DiscardOldestPolicy()//DiscardOldestPolicy 丢弃队列最前面的任务，重新尝试执行任务。

        );
        for (int i = 0; i <20; i++) {
            executorService.execute(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "来人了，出来接客了。。。。。");
            });
        }
    }

    public static void main3(String[] args) {
        ExecutorService executorService = new ThreadPoolExecutor(
                2, //核心
                5,//最大
                2, //空闲存活时间
                TimeUnit.SECONDS,//时间单位
                new ArrayBlockingQueue<>(3),//阻塞队列
                Executors.defaultThreadFactory(), //线程工厂
                new RejectedExecutionHandler() {//拒绝策略
                    @Override
                    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {

                        System.out.println("这个是我自定义的拒绝策略。。。。");
                    }
                }
        );
    }


    //Executors 创建线程 Executors 创建的线程池不推荐 因为无限队列没有边界  推荐原始new ThreadPoolExecutor
    public static void main5(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);//创建固定线程数5
        //ExecutorService executorService = Executors.newCachedThreadPool();//可缓存线程池
        //ExecutorService executorService = Executors.newSingleThreadExecutor();//创建单例线程 //一个一个的来

        for (int i = 0; i < 100; i++) {

            executorService.execute(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "来人了，出来接客了。。。。。");
            });
        }


        executorService.shutdown();//一般线程池不需要关掉
    }

    @GetMapping("/ces")
    public String ces() {

        ExecutorService executorService = Executors.newSingleThreadExecutor();//这种不能解决并发

        executorService.execute(() -> {
            Integer code = employeeMapper.selectCount(Wrappers.lambdaQuery(Employee1.class)
                    .eq(Employee1::getName, "黄"));

            if(code>0){
                return ;
            }

            Employee1 employee1 = new Employee1();
            employee1.setName("黄");
            employeeMapper.insert(employee1);

            System.out.println(Thread.currentThread().getName() + "来人了，出来接客了。。。。。");
        });


        return "1";
    }


}
