package com.miaoyi.mq.consumer.delay;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(
        topic = "01-delay",
        consumerGroup = "my-delay-group"
)
public class DelayConsumer implements RocketMQListener<String> {//这里泛型发送什么就接收什么
    @Override
    public void onMessage(String message) {
         System.out.println("消费消息延迟"+message);
    }
}
