package com.miaoyi.mq.consumer.filter;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(
        topic = "boot-filter",
        consumerGroup = "my-filtersql-group",
        selectorType = SelectorType.SQL92,
        selectorExpression = "age>18" //对应标签
)
public class FilterSqlConsumer implements RocketMQListener<String> {//这里泛型发送什么就接收什么

    @Override
    public void onMessage(String message) {
        System.out.println("消费消息过滤sql" + message);
    }
}
