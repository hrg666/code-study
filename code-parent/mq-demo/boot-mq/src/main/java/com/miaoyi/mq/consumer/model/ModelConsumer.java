package com.miaoyi.mq.consumer.model;

import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(
        topic = "boot-model",
        consumerGroup = "springboot-group",
        messageModel = MessageModel.CLUSTERING //代表集群模式
        //messageModel = MessageModel.BROADCASTING //代表广播模式
)
public class ModelConsumer implements RocketMQListener<String> {//这里泛型发送什么就接收什么
    @Override
    public void onMessage(String message) {
         System.out.println("消费1消息"+message);
    }
}
