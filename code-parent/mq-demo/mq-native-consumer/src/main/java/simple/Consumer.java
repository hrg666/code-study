package simple;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class Consumer {
    public static void main(String[] args) throws Exception{
        //创建一个拉取消息的消费者对象
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("wolfcode-consumer-group");
        //设置名字地址
        consumer.setNamesrvAddr("127.0.0.1:9876");

        /**
         * topic 要订阅的主题
         * subExpression 表达式 这个表达式是用于消息过滤 *不过滤
         */
        consumer.subscribe("01-hello", "*");
        consumer.setMessageListener(new MessageListenerConcurrently() {
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext context) {
                for (MessageExt messageExt : list) {
                    System.out.println("消费线程:"+Thread.currentThread().getName()+",消息ID:"+messageExt.getMsgId()+",消息内容:"+new String(messageExt.getBody()));
                }
                //返回一个成功的状态 失败的话可以重试
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;

                //下面代码 失败重试 成功提交成功
              /* try {
                   //增加积分
                   return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
               }catch (Exception e){
                   e.printStackTrace();
                   return ConsumeConcurrentlyStatus.RECONSUME_LATER;
               }*/

            }
        });
        //启动消费者
        consumer.start();
    }
}
