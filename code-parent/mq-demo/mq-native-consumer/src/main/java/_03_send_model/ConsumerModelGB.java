package _03_send_model;


import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;

import java.util.List;

//广播模式
public class ConsumerModelGB {

    public static void main(String[] args) throws Exception{
        //创建一个拉取消息的消费者对象
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("rocketmq-consumer-group");
        //consumer.setMessageModel(MessageModel.CLUSTERING); //代表集群模式 集群模式就是只能一个接收到消息(消费)
        consumer.setMessageModel(MessageModel.BROADCASTING); //代表广播模式  广播模式就是所有都能接受(消费)
        //设置名字地址
        consumer.setNamesrvAddr("127.0.0.1:9876");

        //绑定消息的主题
        /**
         * topic 要订阅的主题
         * subExpression 表达式 这个表达式是用于消息过滤
         */
        consumer.subscribe("02-model", "*");
        //消费者监听处理消息方法
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println("消费者1-消息的内容为" + new String(msg.getBody()) );
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        //启动消费者
        consumer.start();
    }
}
