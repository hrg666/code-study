package _06_tag_sql;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.MessageSelector;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

//过滤消息
public class ConsumerSql {

    public static void main(String[] args) throws Exception{
        //创建一个拉取消息的消费者对象
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("rocketmq-consumer-group77");
        //设置名字地址
        consumer.setNamesrvAddr("127.0.0.1:9876");

        //绑定消息的主题
        /**
         * topic 要订阅的主题
         * subExpression 表达式 这个表达式是用于消息过滤
         */
        //使用sql过滤
        MessageSelector messageSelector = MessageSelector.bySql("age > 16");
        consumer.subscribe("07-tag-sql", messageSelector);
        //消费者监听处理消息方法
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println("消息的内容为" + new String(msg.getBody()) );
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        //启动消费者
        consumer.start();
    }
}
