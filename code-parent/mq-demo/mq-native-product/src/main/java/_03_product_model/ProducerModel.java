package _03_product_model;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;

public class ProducerModel {
    public static void main(String[] args) throws Exception{
        //1 创建一个生产者对象, 并且指定一个生产者组
        DefaultMQProducer producer = new DefaultMQProducer("wolfcode-producer-group");
        //2 设置名字服务的地址
        producer.setNamesrvAddr("127.0.0.1:9876");
        //3 启动生产者
        producer.start();
        //4 创建一个消息
        Message message = new Message("02-model", "我的消费模式是什么".getBytes("utf-8"));
        //5 发送消息
        producer.send(message);
        //6 关闭连接
        producer.shutdown();
    }
}
