

### RocketMQ 概念

![01 RocketMQ的核心概念](images/01%20RocketMQ%E7%9A%84%E6%A0%B8%E5%BF%83%E6%A6%82%E5%BF%B5.png)

### RocketMQ-widows安装

解压rocketmq-4.5.1 
修改再conf  broker.conf里的文件
增加两条数据到后面
enablePropertyFilter=true
namesrvAddr=127.0.0.1:9876
运行流程
RocketMQ:高性能、高吞吐量的分布式消息中间件
       进入目录rocketmq/bin
     1.jvm内存配置不足记得修改runserver.cmd和runbroker.cmd
     2.点击启动：mqnamesrv.cmd 
     3.黑窗口启动启动：mqbroker.cmd -c ../conf/broker.conf   (两个都需要启动)
     4.启动控制台application.properties 可修改端口和连接信息
        java -jar rocketmq-console-ng-1.0.1.jar

#### 启动问题：

删除掉这个文件

![image-20231118231205663](images/image-20231118231205663.png)



### RocketMQ安装步骤

1.将压缩包上传服务器,把`rocketmq-all-4.4.0-bin-release.zip`拷贝到`/usr/local/software`

2.使用解压命令进行解压到`/usr/local`目录

```shell
unzip /usr/local/software/rocketmq-all-4.4.0-bin-release.zip -d /usr/local
```

3.软件文件名重命名

```shell
mv  /usr/local/rocketmq-all-4.4.0-bin-release/  /usr/local/rocketmq-4.4/
```

4.设置环境变量

```shell
export JAVA_HOME=/usr/local/jdk1.8
export ROCKETMQ_HOME=/usr/local/rocketmq-4.4
export PATH=$JAVA_HOME/bin:$ROCKETMQ_HOME/bin:$PATH
```

5.修改脚本中的JVM相关参数,修改文件如下

```shell
vi  /usr/local/rocketmq-4.4/bin/runbroker.sh
vi  /usr/local/rocketmq-4.4/bin/runserver.sh
```

修改启动参数配置

```shell
JAVA_OPT="${JAVA_OPT} -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m"
```

6.修改配置文件

```shell
 vi /usr/local/rocketmq-4.4/conf/broker.conf
```

新增配置如下:

**![image-20201209150706832](图片/image-20201209150753513.png)**

7.启动NameServer
```shell
# 1.启动NameServer
nohup sh mqnamesrv &
# 2.查看启动日志
tail -f ~/logs/rocketmqlogs/namesrv.log
```

8.启动Broker

```shell
#1.启动Broker
nohup sh mqbroker -n 部署的IP地址:9876 -c /usr/local/rocketmq-4.4/conf/broker.conf &
#2.查看启动日志
tail -f ~/logs/rocketmqlogs/broker.log 
```

9.使用命令查看是否开启成功

```shell
jps
```

需要看到`NamesrvStartup`和`BrokerStartup`这两个进程

### RocketMQ操作

#### 原生操作

概念：一个组可以多个消费者  一个组也可以多个生产者

里面 默认4个队列

在nameserve 找broke信息找到对应的topic 进行发送消息 进行消费消息



##### 简单发送

订阅的主题一样就可以消费 不需要同个组

生产者

```java
public static void main(String[] args) throws Exception{
        //1 创建一个生产者对象, 并且指定一个生产者组
        DefaultMQProducer producer = new DefaultMQProducer("wolfcode-producer-group");
        //2 设置名字服务的地址
        producer.setNamesrvAddr("127.0.0.1:9876");
        //3 启动生产者
        producer.start();
        //4 创建一个消息
        Message message = new Message("01-hello", "hello,rocketmq4".getBytes("utf-8"));
        //5 发送消息
        producer.send(message);
        //6 关闭连接
        producer.shutdown();
  }
```

消费者

```java
 public static void main(String[] args) throws Exception{
        //创建一个拉取消息的消费者对象
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("wolfcode-consumer-group");
        //设置名字地址
        consumer.setNamesrvAddr("127.0.0.1:9876");

        /**
         * topic 要订阅的主题
         * subExpression 表达式 这个表达式是用于消息过滤 *不过滤
         */
        consumer.subscribe("01-hello", "*");
        consumer.setMessageListener(new MessageListenerConcurrently() {
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext context) {
                for (MessageExt messageExt : list) {
                    System.out.println("消费线程:"+Thread.currentThread().getName()+",消息ID:"+messageExt.getMsgId()+",消息内容:"+new String(messageExt.getBody()));
                }
                //返回一个成功的状态 失败的话可以重试
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;

                //下面代码 失败重试 成功提交成功
              /* try {
                   //增加积分
                   return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
               }catch (Exception e){
                   e.printStackTrace();
                   return ConsumeConcurrentlyStatus.RECONSUME_LATER;
               }*/

            }
        });
        //启动消费者
        consumer.start();
    }
```

##### 发送消息的方式

同步消息 、异步消息、 一次性消息 三种模式

![发送方式](images/%E5%8F%91%E9%80%81%E6%96%B9%E5%BC%8F.png)

##### 上面的发送就是同步

同步需要等mq持久化到磁盘

##### 异步发送

异步不需要等mq持久化到磁盘 就可以执行下一步操作

```java
 //异步发送消息
    public static void main(String[] args) throws Exception{
        //1 创建一个生产者对象, 并且指定一个生产者组
        DefaultMQProducer producer = new DefaultMQProducer("wolfcode-producer-group");
        //2 设置名字服务的地址
        producer.setNamesrvAddr("127.0.0.1:9876");
        //3 启动生产者
        producer.start();
        //4 创建一个消息
        Message message = new Message("01-send-asy", "hello,rocketmq-asy".getBytes("utf-8"));
        //5 发送消息
        producer.send(message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("onSuccess");
            }

            @Override
            public void onException(Throwable throwable) {
                System.out.println("onException");
            }
        });
        Thread.sleep(2000);
        //6 关闭连接
        producer.shutdown();
    }
```

##### 一次性发送消息

更异步一样 不过没有回调

```java
 //发送一次性消息
    public static void main(String[] args) throws Exception{
        //1 创建一个生产者对象, 并且指定一个生产者组
        DefaultMQProducer producer = new DefaultMQProducer("wolfcode-producer-group");
        //2 设置名字服务的地址
        producer.setNamesrvAddr("127.0.0.1:9876");
        //3 启动生产者
        producer.start();
        //4 创建一个消息
        Message message = new Message("01-send-one", "hello,rocketmq-one".getBytes("utf-8"));
        //5 发送消息
        producer.sendOneway(message);
        //6 关闭连接
        producer.shutdown();
    }
```



#### 消费模式

分为集群模式 和广播模式

集群模式：多个消费者只能有一个消费者 消费这条消息

广播模式：多个消费者同时消费这条消息



广播模式业务：比如注册完用户 同时发送邮件和发送短信 可以两个消费者去消费

集群模式业务：比如下单，由多个消费者去抢，并只能消费一次



##### 集群模式

```java
//集群模式
public class ConsumerModelGQ {

    public static void main(String[] args) throws Exception{
        //创建一个拉取消息的消费者对象
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("rocketmq-consumer-group");
        consumer.setMessageModel(MessageModel.CLUSTERING); //代表集群模式 集群模式就是只能一个接收到消息(消费)
        //consumer.setMessageModel(MessageModel.BROADCASTING); //代表广播模式  广播模式就是所有都能接受(消费)
        //设置名字地址
        consumer.setNamesrvAddr("127.0.0.1:9876");

        //绑定消息的主题
        /**
         * topic 要订阅的主题
         * subExpression 表达式 这个表达式是用于消息过滤
         */
        consumer.subscribe("02-model", "*");
        //消费者监听处理消息方法
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println("消费者2-消息的内容为" + new String(msg.getBody()) );
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        //启动消费者
        consumer.start();
    }
}

```

测试时启动两个以上消费者

##### 广播模式

```java
//集群模式
public class ConsumerModelGQ {

    public static void main(String[] args) throws Exception{
        //创建一个拉取消息的消费者对象
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("rocketmq-consumer-group");
        //consumer.setMessageModel(MessageModel.CLUSTERING); //代表集群模式 集群模式就是只能一个接收到消息(消费)
        consumer.setMessageModel(MessageModel.BROADCASTING); //代表广播模式  广播模式就是所有都能接受(消费)
        //设置名字地址
        consumer.setNamesrvAddr("127.0.0.1:9876");

        //绑定消息的主题
        /**
         * topic 要订阅的主题
         * subExpression 表达式 这个表达式是用于消息过滤
         */
        consumer.subscribe("02-model", "*");
        //消费者监听处理消息方法
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println("消费者2-消息的内容为" + new String(msg.getBody()) );
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        //启动消费者
        consumer.start();
    }
}

```



#### 组的概念

![image-20231119220903010](images/image-20231119220903010.png)

图上：需要两个消费者是广播模式 另外两个是集群模式 

解决方案就是设置两个分组  一个组是广播 一个组是集群



#### 消费方式（了解）

##### 推送消费

由生产者推送给消费者

##### 拉取消费

消费者一直要去拉取是否有消息



推送消费

```java
public class PushConsumer {
    public static void main(String[] args) throws Exception{
        //创建一个拉取消息的消费者对象
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("wolfcode-consumer");
        //设置名字地址
        consumer.setNamesrvAddr("127.0.0.1:9876");
        //指定从哪里开始消费
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
        //绑定消息的主题
        consumer.subscribe("03-pull-push", "*");
        //消费者监听处理消息方法
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println("消费线程:"+Thread.currentThread().getName()+",消息ID:"+msg.getMsgId()+",消息内容:"+new String(msg.getBody()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        //启动消费者
        consumer.start();
    }
}
```

拉取消费

```java
public class PullConsumer {
    public static void main(String[] args) throws Exception{
        //创建一个拉取消息的消费者对象
        DefaultMQPullConsumer consumer = new DefaultMQPullConsumer("wolfcode-consumer");
        //设置名字地址
        consumer.setNamesrvAddr("127.0.0.1:9876");
        //启动消费者
        consumer.start();
        PullResult pullResult = consumer.pull(new MessageQueue("03-pull-push", "broker-a", 0), "*", 0, 1);
        for (MessageExt messageExt : pullResult.getMsgFoundList()) {
            System.out.println("消费线程:"+Thread.currentThread().getName()+",消息ID:"+messageExt.getMsgId()+",消息内容:"+new String(messageExt.getBody()));
        }
        consumer.shutdown();
    }
}
```

**我们以上默认用的都是推送**

#### 延迟消费

生产者

```java
 public static void main(String[] args) throws Exception{
        //1 创建一个生产者对象, 并且指定一个生产者组
        DefaultMQProducer producer = new DefaultMQProducer("wolfcode-producer-group");
        //2 设置名字服务的地址
        producer.setNamesrvAddr("127.0.0.1:9876");
        //3 启动生产者
        producer.start();
        //4 创建一个消息
        Message message = new Message("05-delay", "延迟消费".getBytes("utf-8"));

        // 设置延时等级3,这个消息将在10s之后发送(现在只支持固定的几个时间,详看delayTimeLevel)
        message.setDelayTimeLevel(3);
        //5 发送消息
        producer.send(message);
        //6 关闭连接
        producer.shutdown();
    }
```

消费者一样 对应上主题就行

#### 消息过滤

##### 标签过滤

生产者

```java
  public static void main(String[] args) throws Exception{
        //1 创建一个生产者对象, 并且指定一个生产者组
        DefaultMQProducer producer = new DefaultMQProducer("wolfcode-producer-group");
        //2 设置名字服务的地址
        producer.setNamesrvAddr("127.0.0.1:9876");
        //3 启动生产者
        producer.start();
        //4 创建一个消息
        Message message = new Message("06-tag","TAGA", "标签消费".getBytes("utf-8"));
        //5 发送消息
        producer.send(message);
        //6 关闭连接
        producer.shutdown();
    }
```

消费者

```java
  public static void main(String[] args) throws Exception{
        //创建一个拉取消息的消费者对象
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("rocketmq-consumer-group");
        //设置名字地址
        consumer.setNamesrvAddr("127.0.0.1:9876");

        //绑定消息的主题
        /**
         * topic 要订阅的主题
         * subExpression 表达式 这个表达式是用于消息过滤
         */
        consumer.subscribe("06-tag", "TAGA");
        //consumer.subscribe("06-tag", "TAGA || TAGB || TAGC || Tag");//多个
        //消费者监听处理消息方法
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    //System.out.println("消费线程:"+Thread.currentThread().getName()+",消息ID:"+msg.getMsgId()+",消息内容:"+new String(msg.getBody()));
                    System.out.println("消息的内容为" + new String(msg.getBody()) );
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        //启动消费者
        consumer.start();
    }
```

注意：消费者可以多个消费者 生产者只能一个

##### sql92过滤

注意 需要加配置文件 broker.conf

![image-20231119224948958](images/image-20231119224948958.png)



生产者：

```java
 public static void main(String[] args) throws Exception{
        //1 创建一个生产者对象, 并且指定一个生产者组
        DefaultMQProducer producer = new DefaultMQProducer("wolfcode-producer-group");
        //2 设置名字服务的地址
        producer.setNamesrvAddr("127.0.0.1:9876");
        //3 启动生产者
        producer.start();
        //4 创建一个消息
        Message message = new Message("07-tag-sql","TAGA", "标签sql消费".getBytes("utf-8"));
        message.putUserProperty("age","20");
        //5 发送消息
        producer.send(message);
        //6 关闭连接
        producer.shutdown();
    }
```

消费者

```java
public static void main(String[] args) throws Exception{
        //创建一个拉取消息的消费者对象
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("rocketmq-consumer-group77");
        //设置名字地址
        consumer.setNamesrvAddr("127.0.0.1:9876");

        //绑定消息的主题
        /**
         * topic 要订阅的主题
         * subExpression 表达式 这个表达式是用于消息过滤
         */
        //使用sql过滤
        MessageSelector messageSelector = MessageSelector.bySql("age > 16");
        consumer.subscribe("07-tag-sql", messageSelector);
        //消费者监听处理消息方法
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println("消息的内容为" + new String(msg.getBody()) );
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        //启动消费者
        consumer.start();
    }
```



### springboot 集成

导入依赖

```java
<dependency>
    <groupId>org.apache.rocketmq</groupId>
    <artifactId>rocketmq-spring-boot-starter</artifactId>
    <version>2.0.3</version>
</dependency>
```

配置生产者

```java
rocketmq:
  name-server: 127.0.0.1:9876
  producer:
    group: my-group
```

实现代码

发送的是同步消息

```java
@RestController
public class HelloController {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @RequestMapping("01-hello")
    public String sendMsg(String message) {
        //第一个参数是  topic+标签组成
        // 如果不想 写标签那么不写即可
        // 要是需要标签则语法 topic+:tag
        //第二个参数 消息内容
        SendResult sendResult = rocketMQTemplate.syncSend("01-boot:", message);//1、发送同步消息
        System.out.println(sendResult.getMsgId());
        System.out.println(sendResult.getSendStatus());
        return "success";
    }
}
```

消费者

```java
@Component
@RocketMQMessageListener(
        topic = "01-boot",
        consumerGroup = "my-consumer"
)
public class HelloConsumer implements RocketMQListener<String> {//这里泛型发送什么就接收什么
    @Override
    public void onMessage(String message) {
         System.out.println("消费消息"+message);
    }
}
```

### 小结：

组可以不同也可以相同  主题一样就可以消费

##### springboot发送模式

一次性消息、同步发送消息、 异步发送消息

```java
 @RequestMapping("01-hello")
    public String sendMsg(String message) {
        
        SendResult sendResult = rocketMQTemplate.syncSend("01-boot:", message);//1、发送同步消息
        return "success";
    }

    /**
     * 消费模式 一次性发送 同步发送 异步发送
     */

    //发送异步消息
    @RequestMapping("02-hello")
    public String sendMsg2(String message) {
        //rocketMQTemplate.syncSend("01-boot:", message);//1、发送同步消息
        //发送异步消息
        rocketMQTemplate.asyncSend("01-boot:", message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("成功");
            }

            @Override
            public void onException(Throwable throwable) {
                System.out.println("失败");

            }
        });

        return "success";
    }

    //一次性发送消息
    @RequestMapping("03-hello")
    public String sendMsg3(String message) {
        //一次性发送消息
        rocketMQTemplate.sendOneWay("01-boot:", message);

        return "success";
```

##### springboot 消费者模式

集群模式 、广播模式

注意：尽量使用不同的组 如果复制了相同的组可能出现不能消费的情况

这两种模式需要在同一个组才能体现

实现

消费者1

```java
@Component
@RocketMQMessageListener(
        topic = "boot-model",
        consumerGroup = "springboot-group",
        messageModel = MessageModel.CLUSTERING //代表集群模式
        //messageModel = MessageModel.BROADCASTING //代表广播模式
)
public class ModelConsumer implements RocketMQListener<String> {//这里泛型发送什么就接收什么
    @Override
    public void onMessage(String message) {
         System.out.println("消费1消息"+message);
    }
}
```

消费者2

```java
@Component
@RocketMQMessageListener(
        topic = "boot-model",
        consumerGroup = "springboot-group",
        messageModel = MessageModel.CLUSTERING //代表集群模式
        //messageModel = MessageModel.BROADCASTING //代表广播模式
)
public class ModelConsumer2 implements RocketMQListener<String> {//这里泛型发送什么就接收什么
    @Override
    public void onMessage(String message) {
         System.out.println("消费2消息"+message);
    }
}
```

只需要设置

 messageModel = MessageModel.CLUSTERING //代表集群模式
messageModel = MessageModel.BROADCASTING //代表广播模式

##### springboot延迟发送消息

生产者

```java
 //发送延迟消息
    @RequestMapping("05-hello")
    public String delay(String message) {

        MessageBuilder<String> messageBuilder = MessageBuilder.withPayload(message);
        //5000是超时时间  2是消息的延迟级别
        //messageDelayLevel=1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h
        rocketMQTemplate.syncSend("01-delay",messageBuilder.build(), 5000,3);
        return "success";
    }
```

消费者

```java
@Component
@RocketMQMessageListener(
        topic = "01-delay",
        consumerGroup = "my-delay-group"
)
public class DelayConsumer implements RocketMQListener<String> {//这里泛型发送什么就接收什么
    @Override
    public void onMessage(String message) {
         System.out.println("消费消息延迟"+message);
    }
}
```

##### springboot消息过滤

标签过滤 、sql过滤

###### 标签过滤

生产者

```java
  //发送标签 sql 过滤消息
    @RequestMapping("06-hello")
    public String filter(String message) {
        rocketMQTemplate.syncSend("01-filter:TagA", message);
        return "success";
    }
```

**只需要在参数的语法加个冒号  xx:tag**

消费者

```java
@Component
@RocketMQMessageListener(
        topic = "01-filter",
        consumerGroup = "my-filter-group",
        selectorType = SelectorType.TAG,
        //selectorType = SelectorType.SQL92,
        selectorExpression = "TagA" //对应标签
)
public class FilterConsumer implements RocketMQListener<String> {//这里泛型发送什么就接收什么
    @Override
    public void onMessage(String message) {
         System.out.println("消费消息过滤"+message);
    }
}

```

**只需要配置：**

selectorType = SelectorType.TAG,  //代表标签
selectorType = SelectorType.SQL92, //代表sql

selectorExpression = "TagA || TagB " //多个

###### sql过滤

生产者

```java
 //发送 sql 过滤消息
    @RequestMapping("07-hello")
    public String filtersql(String message,String age) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("age",age);
        rocketMQTemplate.convertAndSend("boot-filter", message,map);
        return "success";
    }
```

消费者

```java
@Component
@RocketMQMessageListener(
        topic = "boot-filter",
        consumerGroup = "my-filtersql-group",
        selectorType = SelectorType.SQL92,
        selectorExpression = "age>18" //对应标签
)
public class FilterSqlConsumer implements RocketMQListener<String> {//这里泛型发送什么就接收什么

    @Override
    public void onMessage(String message) {
        System.out.println("消费消息过滤sql" + message);
    }
}

```

















