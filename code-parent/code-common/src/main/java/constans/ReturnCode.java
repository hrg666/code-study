package constans;



public interface ReturnCode {

    static final int SUCCESS = 200;

    static final int LOSTPARAM =2;//：缺少必要参数

    static final int INVALID_SIG = 9; // sig校验失败 无效

    static final int NO_BIND = 10; // 绑定失败 无效

    static final int APIINVALIDATION =8;//接口请求时间失效

    static final int BUSINESS_ERROR = 99;//业务异常

    static final int EXCEPTION = -99;//系统异常

    public static final int INVALID_LOGIN_INFO = 98;// 用户名或密码错误

    public static final int NEVER_USED_CODE = -999999;// -999999：假定为永远也不会出现的returnCode


    public static final int CTITICS_SUCC = 200;//  200 成功

	static final int ACTIVITY_NOT_STARTER = 11;//活动未开始

	static final int ACTIVITY_HAS_ENDED = 12;//活动已结束
	static final int IS_LOGIN = 88;//未登录
	static final int IS_LEAVER = 10001;//等级不够
	static final int IS_BRITH = 10002;//不是生日
	static final int ALREADYRECEIVED  = 10003;//已经领取
	static final int NOTSECKILL = 10004;//不符合秒杀
	static final int ALREADYDONE = 10005;//已经办理
	static final int FIRST_ERROR = 101;//异常1
	static final int SECOND_ERROR = 102;//异常2
	static final int THIRD_ERROR = 103;//异常3
	static final int FOURTH_ERROR = 104;//异常4
	static final int ONE_ERROR = 1;//R类里面的code 1表示不正常


}
