package constans;

import cn.hutool.json.JSON;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public interface Const {

    long MAX_MQ_DELAY_MILLS = 6L * 24 * 60 * 60 * 1000;

    // nacos 配置文件缓存
    Map<String, JSON> CONFIG_CACHE = new HashMap<>();

    // 开始游戏线程计划，用于娃娃机参数设置的回调
    Map<String, Thread> GAME_USER_THREAD_MAP = new ConcurrentHashMap<>();

    String HEADER_TOKEN = "X-token";

    String HEADER_CHANNEL = "X-channel";


    // 推送房间内的用户列表
    Integer WWJ_S2C_USER_LIST = 2014;

    // 游戏结果
    Integer WWJ_S2C_PLAY_RESULT = 2009;

    // 客户端发送的游戏结果
    Integer WWJ_C2S_PLAY_RESULT = 11;


    /**
     * 用户粉丝key
     */
    String USER_FANS_KEY = "user:fans:%s";
    /**
     * 用户关注key
     */
    String USER_FOLLOW_KEY = "user:follow:%s";
    /**
     * 用户黑名单key
     */
    String USER_BLACK_KEY = "user:black:%s";
    /**
     * 用户好友key
     */
    String USER_FRIEND_KEY = "user:friend:%s";

    /**
     * 访客列表未读数量key(标记新增的未读访客数量，同一个用户可多次访问)
     */
    String USER_VISIT_UNREAD_COUNT_KEY = "user:visit:unread:count";
    /**
     * 用户被召回发送消息记录key
     */
    String USER_VISIT_RECALL_MSG_KEY = "user:visit:recall:msg:%s:%s";

    /**
     * 粉丝列表最后读取时间key(标记新增的粉丝是否已读)
     */
    String USER_FANS_LAST_READ_TIME_KEY = "user:fans:last:read:time";

    /**
     * 关注上限
     */
    int USER_RELATION_FOLLOW_LIMIT = 2000;
    /**
     * 黑名单上限
     */
    int USER_RELATION_BLACK_LIMIT = 100;

    /********    支付状态类型    ********/
    // 待支付
    Integer PAY_STATUS_PENDING = 0;

    // 已支付
    Integer PAY_STATUS_PAID = 9;

    /********    渠道    ********/
    String CHANNEL_GF = "gf";
    String CHANNEL_MANGO = "mongo";

    /********    用户分组权重类型    ********/
    // 分类(1:充值礼包，2:代抓物，3:积分商品，4:任务)
    Integer USER_GROUP_WEIGHT_TYPE_RECHARGE = 1;
    Integer USER_GROUP_WEIGHT_TYPE_GOODS_TYPE_CLAW = 2;
    Integer USER_GROUP_WEIGHT_TYPE_GOODS_TYPE_POINTS = 3;
    Integer USER_GROUP_WEIGHT_TYPE_TASK = 4;


    /********    支付渠道类型    ********/
    // 支付类型，1：微信， 2：支付宝，3：银行卡 4: 抖音支付
    Integer PAY_CHANNEL_WECHAT = 1;
    Integer PAY_CHANNEL_ALI = 2;
    Integer PAY_CHANNEL_CARD = 3;
    Integer PAY_CHANNEL_DY = 4;
    // 诺诺支付
    Integer PAY_CHANNEL_NN = 5;


    /********    订单类型    ********/
    // 抓取订单
    Integer ORDER_TYPE_CLAW = 1;

    // 积分订单
    Integer ORDER_TYPE_POINTS = 2;

    /********    申述状态    ********/

    Integer APPEAL_STATUS_DISABLE = -1;

    Integer APPEAL_STATUS_ENABLE = 0;
    // 申述中
    Integer APPEAL_STATUS_PENDING = 1;

    // 已解决
    Integer APPEAL_STATUS_AGREE = 2;

    // 已驳回
    Integer APPEAL_STATUS_REJECT = 3;

    // 已超时
    Integer APPEAL_STATUS_TIMEOUT = 4;

    // 关闭
    Integer APPEAL_STATUS_CLOSE = 5;

    /********    充值套餐类型    ********/
    // 1-普通套餐，2-限购套餐，3-每日套餐
    Integer RECHARGE_COMBO_TYPE_NORMAL = 1;
    Integer RECHARGE_COMBO_TYPE_LIMIT_TIMES = 2;
    Integer RECHARGE_COMBO_TYPE_LIMIT_DAY = 3;

    /********    设备类型    ********/
    Integer DEVICE_TYPE_NORMAL = 1;
    // 免费
    Integer DEVICE_TYPE_FREE = 2;


    /********    设备状态    ********/
    // 状态(0: 空闲，1：游戏中, 2:下线)
    Integer DEVICE_STATUS_USABLE = 0;
    Integer DEVICE_STATUS_PLAYING = 1;
    Integer DEVICE_STATUS_OFFLINE = 2;


    /********    商品类型    ********/
    // 代抓物
    Integer GOODS_TYPE_CLAW = 1;

    // 积分兑换商品
    Integer GOODS_TYPE_POINTS = 2;

    // 免费试玩
    Integer GOODS_TYPE_FREE = 3;

    /********    金额交易类型（1：充值，2：运费）    ********/
    Integer FUND_TRADE_TYPE_RECHARGE = 1;
    Integer FUND_TRADE_TYPE_FREIGHT = 2;

    /********    游戏币交易类型（1：充值，2：注册赠送，3：消费; 4：广告奖励; 5:任务奖励）    ********/
    Integer AMOUNT_TRADE_TYPE_RECHARGE = 1;
    Integer AMOUNT_TRADE_TYPE_GIVE = 2;
    Integer AMOUNT_TRADE_TYPE_CONSUMER = 3;
    Integer AMOUNT_TRADE_TYPE_WATCH_AD = 4;
    Integer AMOUNT_TRADE_TYPE_MISSION = 6;
    // 移动积分兑换
    Integer AMOUNT_TRADE_TYPE_CHINA_MOBILE = 7;
    Integer AMOUNT_TRADE_TYPE_RAFFLE = 8;

    /********    用户统计指标    ********/
    // 累计中奖金额（单位：分）
    String USER_TARGET_PRIZE_AMOUNT = "prize_amount";
    // 累计充值金额（单位：分）
    String USER_TARGET_RECHARGE_AMOUNT = "recharge_amount";
    // 累计游戏币
    String USER_TARGET_COINS_IN_TOTAL = "coins_in_total";
    // 累计消费游戏币
    String USER_TARGET_COINS_OUT_TOTAL = "coins_out_total";
    // roi
    String USER_TARGET_ROI = "roi";


    /********    背包状态    ********/
    // 状态(0: 寄存中，1：已过期， 2：已兑奖品, 3: 已兑积分, 4: 已使用)
    Integer BACKPACK_STATUS_SAVE = 0;
    Integer BACKPACK_STATUS_TIMEOUT = 1;
    Integer BACKPACK_STATUS_EXCHANGE_PRIZE = 2;
    Integer BACKPACK_STATUS_EXCHANGE_POINTS = 3;
    Integer BACKPACK_STATUS_USED = 4;


    /********    消息一级分类    ********/
    // 系统通知
    Integer MSG_CATEGORY_SYSTEM = 1;
    // 官方消息
    Integer MSG_CATEGORY_OFFICIAL = 2;


    /********    Nacos dataId    ********/
    String NACOS_DATA_ID_AD_WATCHING_REWARD = "ad_watching_reward";
    String NACOS_DATA_ID_SORT_USER_PARAM = "sort_user_param";


    /********    Redis Key    ********/
    String REDIS_KEY_USER_TOKEN = "user:token:%s:%s";

    String REDIS_KEY_USER_TOKEN_USERID = "user:id:token:%s:%s:%s";
    String REDIS_KEY_PLAYING_USER = "wwj:playing:user:%s";

    String REDIS_KEY_WX_MA_LOGIN_OPENID = "wx:ma:login:openid:%s";

    String REDIS_KEY_PLAY_SESSION_ID = "wwj:play:session:id:%s";

    String REDIS_GAME_LOG_USER = "game:log:user:%s:%s";
    String REDIS_GAME_LOG_SUCCESS_USER = "game:log:success:user:%s:%s";
    String REDIS_GAME_LOG_DEVICE = "game:log:device:%s:%s";
    String REDIS_GAME_LOG_SUCCESS_DEVICE = "game:log:success:device:%s:%s";

    String REDIS_GAME_LOG_DEVICE_PRIZE_TIMES = "game:log:device:prize:times:%s";

    String REDIS_GAME_LOG_USER_PRIZE_TIMES = "game:log:user:prize:times:%s";
    String REDIS_AD_WATCHING_TIMES = "ad:watching:times:%s:%s:%s";

    String REDIS_GAME_LOG_SYNC_ID = "game:log:sync:id";

    // 用户统计指标
    String REDIS_USER_STAT_TARGET = "user:stat:target:%s";

    String REDIS_GAME_PARAM_SETTING = "game:param:setting:%s";

    String REDIS_DEVICE_STATUS_PLAYING = "device:status:playing:%s";
    String REDIS_GAME_CALL_USER = "game:call:user:%s:%s:%s";
    String REDIS_GAME_APPEAL_USER_COUNT = "game:appeal:user:%s:%s";

    String REDIS_MSG_SEND_TIMESTAMP_ALL = "msg:send:timestamp:all:%s";
    String REDIS_MSG_SEND_TIMESTAMP_CHANNEL = "msg:send:timestamp:channel:%s:%s";
    String REDIS_MSG_SEND_TIMESTAMP_USER = "msg:send:timestamp:user:%s:%s";
    String REDIS_MSG_READ_TIMESTAMP = "msg:read:timestamp:%s";
    String REDIS_GAME_AGAIN_PROTECT_USER = "game:again:protect:user:%s";
    String REDIS_RECHARGE_LIMIT_TIMES = "recharge:limit:times:%s";
    String REDIS_RECHARGE_LIMIT_DAY = "recharge:limit:times:%s:%s:%s";
    String REDIS_GAME_CLAW_PRIZE_LOG = "game:claw:prize:log:%s:%s";
    String REDIS_GAME_CLAW_PRIZE_LOG_READ_TIMESTAMP = "game:claw:prize:log:read:timestamp:%s";

    String REDIS_DEVICE_OFFLINE = "wwj:offline:device:%s";
    String REDIS_MQ_TIME_COUNTDOWN = "mq:delay:time:countdown";


    /********    MQ topic    ********/
    String MQ_TOPIC_WEBSOCKET_SEND_MSG = "websocket-send-msg-topic";
    String MQ_TOPIC_GAME_RET = "game-ret-topic";
    String MQ_TOPIC_UNPARK_GAME_THREAD = "unpark-game-thread-topic";
    String MQ_TOPIC_BACKPACK_TIMEOUT = "backpack-timeout-topic";
    String MQ_TOPIC_SEND_MSG = "message-send-topic";
    String MQ_TOPIC_MISSION_FINISH = "mission-finish-topic";
    String MQ_TOPIC_REWARD_RECEIVE = "reward-receive-topic";
    String MQ_TOPIC_AUDIT_SUBMIT = "audit-submit-topic";
    String MQ_TOPIC_AUDIT_FINISH = "audit-finish-topic";
    // 充值成功
    String MQ_TOPIC_RECHARGE_SUCCESS = "recharge-success-topic";


    // 奖励金币类型
    // 注册
    String GIVE_COINS_REGISTER = "register";


    // 标记用户是否是新用户
    String USER_FIRST_REGISTER = "wwj:user:first:reg:%s";
    String CACHE_PAY_TRADE_NO = "wwj:pay:no:%s";
    String CACHE_COINS_TRADE_NO = "wwj:coins:no:%s";
    String CACHE_ORDER_NO = "wwj:order:no:%s";


}
