# websocket集成文档

1.添加依赖

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-websocket</artifactId>
</dependency>
```

2.添加配置

```java
@Configuration
public class WebSocketConfig {
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
```

3.添加websocket处理器

```java
@Setter@Getter
@ServerEndpoint("/{uuid}")
@Component
public class WebSocketServer {
    private Session session;
    public static ConcurrentHashMap<String,WebSocketServer> clients = new ConcurrentHashMap<>();
    @OnOpen
    public void onOpen(Session session, @PathParam( "uuid") String uuid){
        System.out.println("客户端连接===>"+uuid);
        this.session = session;
        clients.put(uuid,this);
    }
    @OnClose
    public void onClose(@PathParam( "uuid") String uuid){
        clients.remove(uuid);
    }
    @OnError
    public void onError(Throwable error) {
        error.printStackTrace();
    }
}
```

给前端发送

```java
@RestController
public class MessagesController {

    @GetMapping("/message")
    //给客户端发送信息
    public String test1(String token,String msg) throws IOException {
        Session session = WebSocketServer.clients.get(token);//根据传过来的token发送
        session.getBasicRemote().sendText(msg); //给客户端发送信息
        return "发送成功";
    }

}
```

