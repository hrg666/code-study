package com.miaoyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSocketApi {
    public static void main(String[] args) {
        SpringApplication.run(WebSocketApi.class,args);
    }
}
