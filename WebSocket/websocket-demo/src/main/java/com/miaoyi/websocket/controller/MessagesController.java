package com.miaoyi.websocket.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.*;
import java.io.IOException;

@RestController
public class MessagesController {

    @GetMapping("/message")
    //给客户端发送信息
    public String test1(String token,String msg) throws IOException {
        Session session = WebSocketServer.clients.get(token);//根据传过来的token发送
        session.getBasicRemote().sendText(msg); //给客户端发送信息
        return "发送成功";
    }

}
