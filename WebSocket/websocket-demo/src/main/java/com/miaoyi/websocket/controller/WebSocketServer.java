package com.miaoyi.websocket.controller;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.concurrent.ConcurrentHashMap;

@Setter
@Getter
@ServerEndpoint("/ws/{token}")  //前端只需要监控 ws://localhost:1005/ws/{token} 这样就会跑过来 token唯一标识
@Component
public class WebSocketServer {

    //多请求的话 会出现线程不安全 所以使用ConcurrentHashMap
  public static ConcurrentHashMap<String, Session> clients = new ConcurrentHashMap<>();

    /**
     * onOpen 方法 当客户端和服务器成功连接的时候触发该方法
     * onClose 方法 当客户端和服务器断开连接的时候触发该方法
     * onError 方法 当客户端和服务器连接出现异常时候触发该方法
     * onMessage 方法 当客户端给服务器发送数据会触发该方法
     */

    @OnOpen
    public void onOpen(Session session, @PathParam("token") String token) {
        System.out.println("客户端连接===>" + token);
        clients.put(token, session);
    }

    @OnMessage
    public void onMessage(@PathParam("token") String token, String msg) {
        System.out.println("收到客户端的信息：" + msg);
        System.out.println("客户标识：" + token);
    }

    @OnClose
    public void onClose(@PathParam("token") String token) {
        clients.remove(token);
    }

    @OnError
    public void onError(Throwable error) {
        error.printStackTrace();
    }
}
